r"""
This module contains a selection of Falcon services
(see documentation for ``m4db.rest.server.app`` for http endpoints) to deal primarily with
querying, adding and updating NEB objects, (i.e. objects of type
:class:`~m4db.orm.NEB`.
"""

import falcon
import traceback
import json
import os

from m4db_database.configuration import read_config_from_environ
from sqlalchemy import and_
from sqlalchemy.orm import aliased

from m4db_database.orm import Model
from m4db_database.orm import RunningStatus
from m4db_database.orm import Project
from m4db_database.orm import Metadata
from m4db_database.orm import DBUser
from m4db_database.orm import NEBCalculationType
from m4db_database.orm import NEBRunData
from m4db_database.orm import NEBReportData
from m4db_database.orm import NEB

from m4db_database.orm import ACCESS_READ
from m4db_database.orm import ACCESS_WRITE

from m4db_database.utilities import uid_to_dir

from m4db_rest_server.global_config import get_global_variables

from m4db_rest_server.services import M4DBService

###############################################################################
###############################################################################
#     NEB GET SERVICES (ones that *DO NOT* change the DB) GO HERE             #
###############################################################################
###############################################################################
from m4db_rest_server.utilities.neb import parse_path_structure_energies


class GetNebByUniqueId(M4DBService):
    r"""
    A class to encapsulate a Falcon REST service to retrive
    :class:`~m4db.orm.Neb` objects from m4db.
    """

    def __init__(self, access_level=ACCESS_READ):
        super().__init__(access_level)

    def on_get(self, req, resp, unique_id):
        r"""
        Retrieve the information associated with a Neb object that has
        the unique id `unique_id`.

        :param req: the Falcon request object.
        :param resp: the Falcon response object
        :param unique_id: the unique identifier of the NEB.
        """
        self.check_access_level(req, resp)
        gcfg = get_global_variables()
        gcfg.logger.debug("Looking of UID: {}".format(unique_id))

        try:
            neb = self.session.query(NEB). \
                filter(NEB.unique_id == unique_id). \
                one_or_none()

            if neb is None:
                resp.status = falcon.HTTP_404
                return
            else:
                resp.status = falcon.HTTP_200
                resp.body = json.dumps(neb.as_dict())

                return
        except:
            # Some sort of error occured, log
            resp.status = falcon.HTTP_500
            gcfg.logger.error("Some error occured, stack trace follows:")
            gcfg.logger.error(traceback.print_exc())
            return


class GetPathRootByStartEndUniqueIds(M4DBService):
    r"""
    A class to encapsulate a Falcon REST service to retrieve an NEB path
    by using the start/end unique ids of the models that constitute the
    start and end model of the NEB paths. The path is a root, i.e. it has no
    parent.
    """
    def __init__(self, access_level=ACCESS_READ):
        super().__init__(access_level)

    def on_get(self, req, resp, unique_id_one, unique_id_two):
        r"""
        Check to see whether an neb with the given start/end unique id exists
        on the system.
        :param req:  the Falcon request object (containing the http post data
                     sent by the user.
        :param resp: the Falcon response object.
        :param unique_id_one: the unique identifier of a model (either the
                              start/end of a path).
        :param unique_id_two: the unique identifier of another model (either
                              the start/end of a path).
        :return: None
        """
        self.check_access_level(req, resp)
        gcfg = get_global_variables()
        model1 = aliased(Model)
        model2 = aliased(Model)
        try:
            path_one = self.session.query(NEB). \
                join(model1, NEB.start_model_id == model1.id). \
                join(model2, NEB.end_model_id == model2.id). \
                filter(and_(model1.unique_id == unique_id_one, model2.unique_id == unique_id_two)). \
                filter(NEB.parent_neb_id == None). \
                one_or_none()
            if path_one is not None:
                resp.status = falcon.HTTP_200
                resp.body = json.dumps(path_one.as_dict())
                return
            path_two = self.session.query(NEB). \
                join(model1, NEB.start_model_id == model1.id). \
                join(model2, NEB.end_model_id == model2.id). \
                filter(and_(model1.unique_id == unique_id_two, model2.unique_id == unique_id_one)). \
                filter(NEB.parent_neb_id == None). \
                one_or_none()
            if path_two is not None:
                resp.status = falcon.HTTP_200
                resp.body = json.dumps(path_two.as_dict())
                return
            resp.status = falcon.HTTP_404
        except:
            resp.status = falcon.HTTP_500
            gcfg.logger.error(traceback.print_exc())


class GetNEBPathStructureEnergiesByUniqueId(M4DBService):

    def __init__(self, access_level=ACCESS_READ):
        super().__init__(access_level)

    def on_get(self, req, resp, unique_id):
        self.check_access_level(req, resp)
        config = read_config_from_environ()
        gcfg = get_global_variables()

        # We don't really need to hit the database for this query
        # The actual neb's location on our file system.
        neb_src_dir = os.path.join(
            config["file_root"], "neb", uid_to_dir(unique_id)
        )

        energy_path_file = os.path.join(
            neb_src_dir, "neb_stdout.txt"
        )

        if not os.path.isfile(energy_path_file):
            gcfg.logger.debug("The NEB path '{}' has no 'neb_stdout.txt' - does it exist?")
            resp.status = falcon.HTTP_404
            return

        path_structure_energies = parse_path_structure_energies(energy_path_file)

        resp.body = json.dumps(path_structure_energies)
        resp.status = falcon.HTTP_200


###############################################################################
###############################################################################
#          MODEL POST SERVICES (ones that change the DB) GO HERE              #
###############################################################################
###############################################################################


###############################################################################
# Falcon service to set model running statuses                                #
###############################################################################

class SetNEBRunningStatus(M4DBService):
    r"""
    A class to encapsulate a Falcon REST service to change the running status
    of a :class:`~m4db_database.orm.NEB` object, i.e. there are not checks as
    to what the existing NEB's running status is, the running status is just
    changed.
    """

    def __init__(self, access_level=ACCESS_READ | ACCESS_WRITE):
        super().__init__(access_level)

    def on_post(self, req, resp):
        r"""
        Update a model's `running_status` using http post data. This data
        is in the form of a JSON string
            {
                "unique_id": "<the_unique_id>",
                "new_status": "<the new status>"
            }

        :param req:  the Falcon request object (containing the http post data
                     sent by the user.
        :param resp: the Falcon response object.
        """
        self.check_access_level(req, resp)
        gcfg = get_global_variables()

        data = req.media

        # Check message
        try:
            unique_id = data["unique_id"]
            new_status = data["new_status"]
        except:
            # Malformed request, user data failed validation
            resp.status = falcon.HTTP_400
            return

        # Get the running status
        running_status = self.session.query(RunningStatus). \
            filter(RunningStatus.name == new_status). \
            one_or_none()
        if running_status is None:
            # Malformed request, user data failed validation
            resp.status = falcon.HTTP_400
            return

        # Get the model with unique id
        model = self.session.query(NEB). \
            filter(NEB.unique_id == unique_id). \
            one_or_none()

        if model is None:
            # Could not find model, 404 (missing resource)
            resp.status = falcon.HTTP_404
            return

        # Set the running status to new_running_status
        try:
            model.running_status = running_status
            self.session.commit()
        except:
            # Some sort of error - log
            gcfg.logger.error("Some error occured, stack trace follows:")
            gcfg.logger.error(traceback.print_exc())
            resp.status = falcon.HTTP_500
            return

        # Success
        resp.status = falcon.HTTP_200
        return


###############################################################################
# Falcon service to add an (unrun) NEB path                                   #
###############################################################################


class AddNotRunNEBPath(M4DBService):
    r"""
    A class to encapsulate a Falcon REST service to add NEB paths.
    """

    def __init__(self, access_level=ACCESS_READ | ACCESS_WRITE):
        super().__init__(access_level)

    def on_post(self, req, resp):
        r"""
        Add an NEB path to the database.

        :param req:  the Falcon request object (containing the http post data
                     sent by the user.
        :param resp: the Falcon response object.

        Note: response returns http errors on following condition

              * 500 - if the start/end point of the path does not exist
                      if the parent path does not exit
                      if the calculation type does not exist
                      if the user name / project does not exist
                      and some other error
        """
        self.check_access_level(req, resp)
        config = read_config_from_environ()
        gcfg = get_global_variables()
        data = req.media

        gcfg.logger.debug("Adding a new NEB path")

        start_model_unique_id = None
        end_model_unique_id = None
        parent_unique_id = None

        if "start_model_unique_id" in data.keys():
            start_model_unique_id = data["start_model_unique_id"]

        if "end_model_unique_id" in data.keys():
            end_model_unique_id = data["end_model_unique_id"]

        if "parent_unique_id" in data.keys():
            parent_unique_id = data["parent_unique_id"]

        if start_model_unique_id is None and end_model_unique_id is None and parent_unique_id is None:
            # All three (start, end, parent) have no value is an error.
            resp.status = falcon.HTTP_400
            return
        if start_model_unique_id is not None and end_model_unique_id is None and parent_unique_id is not None:
            # Start and parent have a value is an error.
            resp.status = falcon.HTTP_400
            return
        if start_model_unique_id is None and end_model_unique_id is not None and parent_unique_id is not None:
            # End and parent have a value is an error.
            resp.status = falcon.HTTP_400
            return

        try:
            project_name = data["project_name"]
            user_name = req.get_header("user_name")
            spring_constant = data["spring_constant"]
            curvature_weight = data["curvature_weight"]
            no_of_points = data["no_of_points"]
            calculation_type = data["calculation_type"]
        except:
            gcfg.logger.debug(traceback.print_exc())
            # Malformed request, user data failed validation
            gcfg.logger.debug("Malformed request")
            resp.status = falcon.HTTP_400
            return

        try:
            user = self.session.query(DBUser).\
                filter(DBUser.user_name == user_name).\
                one()

            project = self.session.query(Project).\
                filter(Project.name == project_name).\
                one()

            metadata = Metadata(
                db_user=user,
                project=project,
                software=None
            )

            calculation_type = self.session.query(NEBCalculationType). \
                filter(NEBCalculationType.name == calculation_type). \
                one_or_none()

            if calculation_type is None:
                # Backend error
                resp.status = falcon.HTTP_500
                gcfg.logger.error(
                    "Could not find calculation type: {}".format(calculation_type)
                )
                return

            running_status = self.session.query(RunningStatus). \
                filter(RunningStatus.name == "not-run"). \
                one_or_none()

            if running_status is None:
                # Backend error
                resp.status = falcon.HTTP_500
                gcfg.logger.error(
                    "Could not find running status: not-run"
                )
                return

            # If there is a start model unique id and an end model unique id.
            if start_model_unique_id is not None and end_model_unique_id is not None:
                # Retrieve the start model
                start_model = self.session.query(Model). \
                    filter(Model.unique_id == start_model_unique_id). \
                    one_or_none()
                if start_model is None:
                    # Backend error
                    resp.status = falcon.HTTP_500
                    gcfg.logger.error(
                        "Could not find start model with unique id: {}".format(start_model_unique_id)
                    )
                    return
                # Retrieve the end model
                end_model = self.session.query(Model). \
                    filter(Model.unique_id == end_model_unique_id). \
                    one_or_none()
                if end_model is None:
                    # Backend error
                    resp.status = falcon.HTTP_500
                    gcfg.logger.error(
                        "Could not find end model with unique id: {}".format(end_model_unique_id)
                    )
                    return
                # Add the new path
                new_neb_path = NEB(
                    spring_constant=spring_constant,
                    curvature_weight=curvature_weight,
                    no_of_points=no_of_points,
                    start_model=start_model,
                    end_model=end_model,
                    neb_calculation_type=calculation_type,
                    neb_run_data=NEBRunData(),
                    neb_report_data=NEBReportData(),
                    running_status=running_status,
                    mdata=metadata
                )
                self.session.add(new_neb_path)
                self.session.commit()
                resp.status = falcon.HTTP_200
                resp.body = json.dumps({"unique_id": new_neb_path.unique_id})
                return

            # If there is a parent path.
            elif parent_unique_id is not None:
                # Retrieve the parent path
                parent_path = self.session.query(NEB). \
                    filter(NEB.unique_id == parent_unique_id). \
                    one_or_none()
                if parent_path is None:
                    # Backend error
                    resp.status = falcon.HTTP_500
                    gcfg.logger.error(
                        "Could not find parent path with unique id: {}".format(parent_unique_id)
                    )
                    return
                # Add the new path
                new_neb_path = NEB(
                    spring_constant=spring_constant,
                    curvature_weight=curvature_weight,
                    no_of_points=no_of_points,
                    start_model=parent_path.start_model,
                    end_model=parent_path.end_model,
                    parent_neb=parent_path,
                    neb_calculation_type=calculation_type,
                    neb_run_data=NEBRunData(),
                    neb_report_data=NEBReportData(),
                    running_status=running_status,
                    mdata=metadata
                )
                self.session.add(new_neb_path)
                self.session.commit()
                resp.status = falcon.HTTP_200
                resp.body = json.dumps({"unique_id": new_neb_path.unique_id})
                return

        except Exception:
            gcfg.logger.error(traceback.print_exc())
            resp.status = falcon.HTTP_500
            return
