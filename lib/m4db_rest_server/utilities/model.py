r"""
This module provides some useful utilites for dealing with
:class: `~m4db.orm.Model` objects such as creating a user readable report
for a model and zipping up a model.
"""

import re
import os
import json
import shutil

from jinja2 import Environment, PackageLoader, select_autoescape

from m4db_rest_server.global_config import get_global_variables

from m4db_database.configuration import read_config_from_environ
from m4db_database.orm import RunningStatus
from m4db_database.orm import Model
from m4db_database.orm import Geometry
from m4db_database.orm import SizeConvention
from m4db_database.orm import Metadata
from m4db_database.orm import Project
from m4db_database.orm import DBUser
from m4db_database.orm import Material
from m4db_database.orm import ModelMaterialAssociation

from m4db_database.utilities import uid_to_dir

from m4db_rest_server.utilities.conversion import convert_to_micron

from sqlalchemy import tuple_

from m4db_rest_server.utilities.validation.model import check_tecplot_mult, check_tecplot


def create_report(model):
    r"""
    This function creates a text report based on the model.
    """
    template_loader = PackageLoader("m4db_rest_server", "templates")
    template_env = Environment(
        loader=template_loader,
        autoescape=select_autoescape(['jinja2'])
    )

    template = template_env.get_template(
        "model_summary_txt.jinja2"
    )

    return template.render(model=model)


def create_model_zip(model_dict):
    r"""
    This function creates a zip file based on the model.
    """
    config = read_config_from_environ()
    gcfg = get_global_variables()

    # Check that the temporary directory exists, if not create it.
    tmp_dir = os.path.join(
        config["file_root"], "temporary"
    )
    if not os.path.isdir(tmp_dir):
        os.makedirs(tmp_dir, exist_ok=True)
    gcfg.logger.debug("tmp_dir: {}".format(tmp_dir))

    # Make another directory inside tmp_dir, based on the model's unique id.
    model_tmp_dir = os.path.join(tmp_dir, model_dict["unique_id"])
    os.makedirs(model_tmp_dir, exist_ok=True)

    gcfg.logger.debug("model_tmp_dir: {}".format(model_tmp_dir))

    # The actual model's location on our file system.
    model_src_dir = os.path.join(
        config["file_root"], "model", uid_to_dir(model_dict["unique_id"])
    )

    gcfg.logger.debug("model_src_dir: {}".format(model_src_dir))

    try:
        # Copy over all the model files
        for file_name in os.listdir(model_src_dir):
            src_file_path = os.path.join(model_src_dir, file_name)
            dst_file_path = os.path.join(model_tmp_dir, file_name)
            if os.path.isfile(src_file_path):
                shutil.copyfile(src_file_path, dst_file_path)

        # Make a (human readable) report summary file
        summary_txt = os.path.join(model_tmp_dir, gcfg.model_summary_txt)
        with open(summary_txt, 'w') as fout:
            fout.write(create_report(model_dict))
            fout.write('\n')
            fout.close()

        # Make a JSON report summary file
        summary_json = os.path.join(model_tmp_dir, gcfg.model_summary_json)
        with open(summary_json, 'w') as fout:
            fout.write(json.dumps(model_dict))
            fout.write('\n')
            fout.close()

        # Zip it up
        zip_file_no_extension = model_tmp_dir
        gcfg.logger.debug("zip_file_no_extension: {}".format(zip_file_no_extension))

        zip_file_with_extension = '{}.zip'.format(zip_file_no_extension)
        gcfg.logger.debug("zip_file_with_extension: {}".format(zip_file_with_extension))

        shutil.make_archive(
            zip_file_no_extension, 'zip', model_tmp_dir
        )

        # Return the name of the zip file created
        return zip_file_with_extension
    finally:
        shutil.rmtree(model_tmp_dir)


def process_materials_temperatures_from_http_get_parameters(params):


    # Here we deal with the materials and temperatures - perform extraction/validation
    regex_material_key = re.compile(r'material([0-9]+)')
    regex_temperature_key = re.compile(r'temperature([0-9]+)')
    materials = []
    temperatures = []

    for key, val in params.items():
        match_material_key = regex_material_key.match(key)
        if match_material_key:
            # Push the *pair* (<material_index>, <material_name>) on the the array
            materials.append((int(match_material_key.group(1)), val))
            continue

        match_temperature_key = regex_temperature_key.match(key)
        if match_temperature_key:
            # Push the *pair* (<temperature_index>, <temperature_name>) on the array
            temperatures.append((int(match_temperature_key.group(1)), float(val)))
            continue

    # Check that the materials/temperatures are the same length
    if len(materials) != len(temperatures):
        raise ValueError("The number of materials supplied does not match the number of temperatures")

    # Sort the materials and temperatures array by index
    materials.sort(key=lambda m: m[0])
    temperatures.sort(key=lambda t: t[0])

    # Check that the materials and temperatures array are aligned by index
    if [t[0] for t in temperatures] != [m[0] for m in materials]:
        raise ValueError("Material indices do not match")

    # Return two arrays, one with material names and another with material temperatures
    return [m[1] for m in materials], [t[1] for t in temperatures]


def rename_tecplot_mult_file(data_dir):
    r"""
    Renames the 'magnetization_mult.tec" file to "magnetization.tec" file if required.
    :param data_dir: the directory in which the "magnetization_mult.tec" file is.
    :return: True if the file was present and renamed or if 'magnetization.tec' is already present, otherwise False.
    """
    gcfg = get_global_variables()
    if check_tecplot_mult(data_dir):
        os.rename(
            os.path.join(data_dir, gcfg.magnetization_mult_tec),
            os.path.join(data_dir, gcfg.magnetization_tec)
        )
        return True
    else:
        return check_tecplot(data_dir)
