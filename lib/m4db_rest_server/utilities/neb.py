r"""
This module provides some useful utilities for dealing with
NEB paths.
"""

import os
import re
import shutil

from m4db_rest_server.global_config import get_global_variables

from m4db_database.configuration import read_config_from_environ
from m4db_database.utilities import uid_to_dir


def create_path_table(neb):
    r"""
    This function will read the NEB data.
    :param neb:
    :return:
    """

def create_neb_zip(neb):
    r"""
    This function creates a zip file based on the neb.
    """
    config = read_config_from_environ()
    gcfg = get_global_variables()

    # Check that the temporary directory exists, if not create it.
    tmp_dir = os.path.join(
        config["file_root"], "temporary"
    )
    if not os.path.isdir(tmp_dir):
        os.makedirs(tmp_dir, exist_ok=True)
    gcfg.logger.debug("tmp_dir: {}".format(tmp_dir))

    # Make another directory inside tmp_dir, based on the neb's unique id.
    neb_tmp_dir = os.path.join(tmp_dir, neb.unique_id)
    os.makedirs(neb_tmp_dir, exist_ok=True)

    gcfg.logger.debug("neb_tmp_dir: {}".format(neb_tmp_dir))

    # The actual neb's location on our file system.
    neb_src_dir = os.path.join(
        config["file_root"], "neb", uid_to_dir(neb.unique_id)
    )

    gcfg.logger.debug("neb_src_dir: {}".format(neb_src_dir))

    try:
        # Copy over all the neb files
        for file_name in os.listdir(neb_src_dir):
            src_file_path = os.path.join(neb_src_dir, file_name)
            dst_file_path = os.path.join(neb_tmp_dir, file_name)
            if os.path.isfile(src_file_path):
                shutil.copyfile(src_file_path, dst_file_path)

        # Zip it up
        zip_file_no_extension = neb_tmp_dir
        gcfg.logger.debug("zip_file_no_extension: {}".format(zip_file_no_extension))

        zip_file_with_extension = '{}.zip'.format(zip_file_no_extension)
        gcfg.logger.debug("zip_file_with_extension: {}".format(zip_file_with_extension))

        shutil.make_archive(
            zip_file_no_extension, 'zip', neb_tmp_dir
        )

        # Return the name of the zip file created
        return zip_file_with_extension
    finally:
        shutil.rmtree(neb_tmp_dir)


def parse_path_structure_energies(file_name):
    r"""
    Parse the path structure energies from the file with file_name.
    :param output_file:
    :return:
    """
    regex_start_parsing = re.compile(r"\s*Parsing\s*:\s*PathStructureEnergies\s*")
    regex_end_parsing = re.compile(r"\s*Parsing\s*:\s*End\s*")
    regex_data_line = re.compile(r"\s*([0-9]+)\s+\s([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?.)\s*")

    BEFORE_PARSING = 1
    PARSING_DATA = 2
    AFTER_PARSING = 3

    path_structure_energies = []

    mode = BEFORE_PARSING
    with open(file_name, "r") as fin:
        for line in fin:
            match_start_parsing = regex_start_parsing.match(line)
            if match_start_parsing and mode == BEFORE_PARSING:
                mode = PARSING_DATA
                continue

            match_data_line = regex_data_line.match(line)
            if match_data_line and mode == PARSING_DATA:
                path_structure_energies.append([
                    int(match_data_line.group(1)), float(match_data_line.group(2))
                ])
                continue

            match_data_line = regex_end_parsing.match(line)
            if match_data_line and mode == PARSING_DATA:
                mode = AFTER_PARSING
                continue

    return path_structure_energies
