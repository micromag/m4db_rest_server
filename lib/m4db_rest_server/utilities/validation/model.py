r"""
This file contains a set of routines that will validate a model.
"""
import os

from m4db_rest_server.global_config import get_global_variables

from m4db_database.orm import Model
from m4db_database.orm import ModelRunData
from m4db_database.orm import ModelReportData

from functools import reduce


def core_model_run_data(session, unique_id):
    r"""
    Returns the core model run data boolean values.
    :param session: the database session.
    :return: a tuple of boolean values corresponding to the minimum information
             about a Model in ModelRunData.
    """
    return session.query(
        ModelRunData.has_script,
        ModelRunData.has_stdout,
        ModelRunData.has_stderr,
        ModelRunData.has_energy_log,
        ModelRunData.has_tecplot,
        ModelRunData.has_dat
    ) \
        .join(Model, Model.model_run_data_id == ModelRunData.id) \
        .filter(Model.unique_id == unique_id) \
        .one_or_none()


def is_core_model_run_data_complete(session, unique_id):
    r"""
    Checks to see whether the core model run data in the database is complete.
    :param session: the database session
    :param unique_id: the unique id of the model
    :return: True if the set of core data for a model is complete, otherwise False
    """
    return reduce(lambda a, b: a and b, core_model_run_data(session, unique_id))


def core_minimum_file_set(data_dir):
    r"""
    Checks to see if the model data contains a minimum file set.
    :param data_dir: the directory in which the model data lives.
    :return: True if the model is valid, otherwise false
    """
    return (
        check_script(data_dir),
        check_stdout(data_dir),
        check_stderr(data_dir),
        check_energy_log(data_dir),
        check_tecplot(data_dir),
        check_tecplot(data_dir),
        check_dat(data_dir)
    )


def is_core_minimum_file_set_complete(data_dir):
    r"""
    Checks to see if the model data contains the core minimum file set.
    :param data_dir: the directory in which the model data lives.
    :return: True if the minimum files required for a valid model exists.
    """
    return reduce(lambda a, b: a and b, core_minimum_file_set(data_dir))


def check_script(data_dir):
    r"""
    Check to see if the script that generated a model is present.
    :param data_dir: the directory in which the script should reside.
    :return: True if the generation script is present, otherwise false.
    """
    gcfg = get_global_variables()
    return os.path.isfile(os.path.join(data_dir, gcfg.model_script_merrill))


def check_stdout(data_dir):
    r"""
    Check to see if the standard out for a model is present.
    :param data_dir: the directory in which the standard model should be.
    :return: True if the standard model is present, otherwise False.
    """
    gcfg = get_global_variables()
    return os.path.isfile(os.path.join(data_dir, gcfg.model_stdout_txt))


def check_stderr(data_dir):
    r"""
    Check to see if the standard error for a model is present.
    :param data_dir: the directory in which the standard error should be.
    :return: True if the standard error is present, otherwise False.
    """
    gcfg = get_global_variables()
    return os.path.isfile(os.path.join(data_dir, gcfg.model_stdout_txt))


def check_energy_log(data_dir):
    r"""
    Check to see if the energy log for a model is present.
    :param data_dir: the directory in which the energy log should be.
    :return: True if the energy log is present, otherwise False.
    """
    gcfg = get_global_variables()
    return os.path.isfile(os.path.join(data_dir, gcfg.magnetization_log))


def check_tecplot(data_dir):
    r"""
    Check to see if the tecplot file for a model is present.
    :param data_dir: the directory in which the tecplot file should be.
    :return: True if the tecplot file is present, otherwise False.
    """
    gcfg = get_global_variables()
    return os.path.isfile(os.path.join(data_dir, gcfg.magnetization_tec))


def check_tecplot_mult(data_dir):
    r"""
    Check to see if the tecplot mult file for a model is present.
    :param data_dir: the directory in which the tecplot mult file should be.
    :return: True if the tecplot mult file is present, otherwise False.
    """
    gcfg = get_global_variables()
    return os.path.isfile(os.path.join(data_dir, gcfg.magnetization_mult_tec))


def check_dat(data_dir):
    r"""
    Check to see if the dat file for a model is present.
    :param data_dir: the directory in which the dat file should be.
    :return: True if the dat file is present, otherwise False.
    """
    gcfg = get_global_variables()
    return os.path.isfile(os.path.join(data_dir, gcfg.magnetization_dat))


def check_model_exists(session, unique_id):
    r"""
    A function that will check whether a model of a given unique_id exists in the database.
    :param session: the database session
    :param unique_id: The unique Id of the model to check.
    :return: True if the model exists in the database otherwise false.
    """

    if session.query(Model.unique_id).filter(Model.unique_id == unique_id).one_or_none():
        return True
    return False
