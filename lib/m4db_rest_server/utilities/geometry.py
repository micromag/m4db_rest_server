r"""
This module provides some useful utilities for dealing with
geometries.
"""

import os
import shutil
import base64

from m4db_database.configuration import read_config_from_environ
from m4db_database.utilities import uid_to_dir


def create_geometry_zip(unique_id):
    r"""
    This function creates a zip file based on the geometry.
    """
    config = read_config_from_environ()

    # Check that the temporary directory exists, if not create it.
    tmp_dir = os.path.join(
        config["file_root"], "temporary"
    )
    if not os.path.isdir(tmp_dir):
        os.makedirs(tmp_dir, exist_ok=True)

    # Make another directory inside tmp_dir, based on the geometry's unique id.
    geometry_tmp_dir = os.path.join(tmp_dir, unique_id)
    os.makedirs(geometry_tmp_dir, exist_ok=True)

    # The actual geometry's location on our file system.
    geometry_src_dir = os.path.join(
        config["file_root"], "geometry", uid_to_dir(unique_id)
    )

    try:
        # Copy over all the geometry files
        for file_name in os.listdir(geometry_src_dir):
            src_file_path = os.path.join(geometry_src_dir, file_name)
            dst_file_path = os.path.join(geometry_tmp_dir, file_name)
            if os.path.isfile(src_file_path):
                shutil.copyfile(src_file_path, dst_file_path)

        # Zip it up
        zip_file_no_extension = geometry_tmp_dir
        zip_file_with_extension = '{}.zip'.format(zip_file_no_extension)
        shutil.make_archive(
            zip_file_no_extension, 'zip', geometry_tmp_dir
        )

        # Return the name of the zip file created
        return zip_file_with_extension
    finally:
        shutil.rmtree(geometry_tmp_dir)


def decode_geometry(base64_data, file_name):
    r"""
    Decodes the data from base
    :param base64_data:
    :param file_name:
    :return:
    """
    with open(file_name, "wb") as fout:
        fout.write(base64.b64decode(base64_data))
