r"""
Utility functions to convert units.
"""

from decimal import Decimal


def convert_to_micron(size, unit):
    r"""
    Converts the input size to microns, if str_version is set to true then a
    string version of the unit is produced.

    :param size:      the size (in floating point) to convert to micron
    :param unit:      the unit that `size` is given in

    :returns: the input `size` float converted to micron.
    """
    if type(size) is str:
        fsize = float(size)
    else:
        fsize = size

    if unit == 'm':
        temp_size = 1E6*fsize
    elif unit == 'cm':
        temp_size = 1E4*fsize
    elif unit == 'mm':
        temp_size = 1E3*fsize
    elif unit == 'um':
        temp_size = fsize
    elif unit == 'nm':
        temp_size = 1E-3*fsize
    elif unit == 'fm':
        temp_size = 1E-6*fsize
    elif unit == 'am':
        temp_size = 1E-9*fsize
    else:
        raise ValueError("Unknown size unit")

    return Decimal("{:10.5f}".format(round(temp_size, 5)))


def convert_to_celsius(temperature, unit):
    r"""
    Converts the input temperature to celsius

    :param temperature: the temperature (in floating point) to convert to micron
    :param unit:        the unit that `temperature` is given in

    :returns: the input `temperature` float converted to micron.
    """
    if type(temperature) is str:
        ftemperature = float(temperature)
    else:
        ftemperature = temperature

    if unit == 'C':
        temp_temperature = ftemperature
    elif unit == 'K':
        temp_temperature = ftemperature - 273.15
    elif unit == 'F':
        temp_temperature = (ftemperature - 32.0)*(5.0/9.0)
    else:
        raise ValueError("Unknown temperature unit")

    return Decimal("{:8.3f}".format(round(temp_temperature, 3)))
