import falcon
import json
import traceback

from m4db_database.orm import Project, ACCESS_ADMIN

from m4db_rest_server.global_config import get_global_variables

from m4db_rest_server.services import M4DBService

###############################################################################
# Falcon service to retrieve models by a project name                         #
###############################################################################


class GetProjectByName(M4DBService):
    r"""
    A class to encapsulate a Falcon REST service to retrive
    :class:`~m4db.orm.Project` objects from m4db.
    """

    def __init__(self, access_level=ACCESS_ADMIN):
        super().__init__(access_level)

    def on_get(self, req, resp, name):
        r"""
        Retrieve the information associated with a Model object that has
        the unique id `unique_id`.

        :param req: the Falcon request object.
        :param resp: the Falcon response object
        :param name: the name of the project.
        """
        self.check_access_level(req, resp)
        gcfg = get_global_variables()

        try:
            project = self.session.query(Project). \
                filter(Project.name == name). \
                one_or_none()

            if project is None:
                resp.status = falcon.HTTP_404
                return
            else:
                resp.status = falcon.HTTP_200
                resp.body = json.dumps(project.as_dict())

                return
        except:
            # Some sort of error occured, log
            resp.status = falcon.HTTP_500
            gcfg.logger.error("Some error occured, stack trace follows:")
            gcfg.logger.error(traceback.print_exc())
            return


class GetAllProject(M4DBService):

    def __init__(self, access_level=ACCESS_ADMIN):
        super().__init__(access_level)

    def on_get(self, req, resp):
        self.check_access_level(req, resp)

        gcfg = get_global_variables()

        dbsession = self.session
        project_query = dbsession.query(Project)
        projects = project_query.all()

        try:
            if projects == []:
                resp.status = falcon.HTTP_404
                return
            elif projects is None:
                resp.status = falcon.HTTP_404
                return
            else:
                resp.status = falcon.HTTP_200
                project_dicts = [project.as_dict() for project in projects]
                resp.body = json.dumps(project_dicts)
                return
        except:
            # Some sort of error occurred, log
            gcfg.logger.error("Some error occurred, stack trace follows:")
            gcfg.logger.error(traceback.print_exc())
            resp.status = falcon.HTTP_500
            return


class NewProject(M4DBService):

    def __init__(self, access_level=ACCESS_ADMIN):
        super().__init__(access_level)

    def on_post(self, req, resp):
        self.check_access_level(req, resp)

        gcfg = get_global_variables()

        data = req.media

        try:
            new_project = Project()
            if "name" not in data.keys():
                # Malformed request, user data failed validation
                resp.status = falcon.HTTP_400
                return
            else:
                new_project.name = data["name"]

            if "description" not in data.keys():
                # Malformed request, user data failed validation
                resp.status = falcon.HTTP_400
                return
            else:
                new_project.description = data["description"]

            self.session.add(new_project)
            self.session.commit()
            return

        except:
            # Some sort of error occurred, log
            gcfg.logger.error("Some error occurred, stack trace follows:")
            gcfg.logger.error(traceback.print_exc())
            resp.status = falcon.HTTP_500
            return
