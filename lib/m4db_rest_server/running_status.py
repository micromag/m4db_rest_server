import falcon
import json
import traceback

from m4db_database.orm import RunningStatus, ACCESS_ALL

from m4db_rest_server.global_config import get_global_variables

from m4db_rest_server.services import M4DBService


class GetAllRunningStatuses(M4DBService):

    def __init__(self, access_level=ACCESS_ALL):
        super().__init__(access_level)

    r"""
    A class to encapsulate a Falcon REST service to retrieve all running
    statuses (of type :class:`~m4db.orm.RunningStatus`) from m4db.
    """
    def on_get(self, req, resp):
        r"""
        Retrieve all the running statuses held within the m4db database.
        """
        self.check_access_level(req, resp)

        gcfg = get_global_variables()
        
        running_statuses = self.session.query(RunningStatus).all()

        try:
            if running_statuses == []:
                resp.status = falcon.HTTP_404
                return
            elif running_statuses is None:           
                resp.status = falcon.HTTP_404
                return
            else:
                resp.status = falcon.HTTP_200
                running_status_dicts = [running_status.as_dict() for running_status in running_statuses]
                resp.body = json.dumps(running_status_dicts)
                return
        except:
            # Some sort of error occurred, log
            gcfg.logger.error("Some error occurred, stack trace follows:")
            gcfg.logger.error(traceback.print_exc())
            resp.status = falcon.HTTP_500
            return