r"""
This module contains a selection of Falcon services
(see documentation for ``m4db.rest.server.app`` for http endpoints) to deal primarily with
querying, adding and updating model objects, (i.e. objects of type
:class:`~m4db.orm.Model`.
"""
import re
import os
import json
import falcon
import traceback
import mimetypes

from decimal import Decimal

import numpy as np
from m4db_database.utilities import uid_to_dir

from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.orm.exc import MultipleResultsFound

from sqlalchemy.orm import aliased

from sqlalchemy import func
from sqlalchemy import text
from sqlalchemy import update

from m4db_database.orm import Model
from m4db_database.orm import Unit
from m4db_database.orm import RandomField
from m4db_database.orm import UniformField
from m4db_database.orm import ModelField
from m4db_database.orm import ModelRunData
from m4db_database.orm import ModelReportData
from m4db_database.orm import RunningStatus
from m4db_database.orm import Geometry
from m4db_database.orm import SizeConvention
from m4db_database.orm import Project
from m4db_database.orm import Material
from m4db_database.orm import ModelMaterialAssociation
from m4db_database.orm import Metadata
from m4db_database.orm import DBUser
from m4db_database.orm import ModelMaterialsText

from m4db_core.db.query.model.query_functions import model_by_unique_id
from m4db_core.db.query.model.query_functions import model_quants_by_mgst
from m4db_core.db.query.model.query_functions import models_by_mgst
from m4db_core.db.query.model.query_functions import models_unique_ids_by_mgst

from m4db_rest_server.utilities.model import create_model_zip

from m4db_rest_server.utilities.validation.model import check_model_exists

from m4db_rest_server.global_config import get_global_variables
from m4db_core.unit_conversion import convert_to_micron

from m4db_database.configuration import read_config_from_environ

from m4db_database.orm import ACCESS_ADMIN
from m4db_database.orm import ACCESS_READ
from m4db_database.orm import ACCESS_WRITE

from m4db_rest_server.services import M4DBService

from m4db_rest_server.io.tecplot import read_tecplot_file

###############################################################################
###############################################################################
#     MODEL GET SERVICES (ones that *DO NOT* change the DB) GO HERE           #
###############################################################################
###############################################################################

###############################################################################
# Falcon service to retrieve models by a unique ID.                           #
###############################################################################

from lib.m4db_rest_server.utilities.model import process_materials_temperatures_from_http_get_parameters


class GetModelByUniqueId(M4DBService):
    r"""
    A class to encapsulate a Falcon REST service to retrive
    :class:`~m4db.orm.Model` objects from m4db.
    """

    def __init__(self, access_level=ACCESS_READ):
        super().__init__(access_level)

    def on_get(self, req, resp, unique_id):
        r"""
        Retrieve the information associated with a Model object that has
        the unique id `unique_id`.

        :param req: the Falcon request object.
        :param resp: the Falcon response object
        :param unique_id: the unique identifier of the model.
        """
        self.check_access_level(req, resp)

        gcfg = get_global_variables()

        try:
            model_dict = model_by_unique_id(self.session, unique_id)
            self.session.commit()

            if model_dict is None:
                resp.status = falcon.HTTP_404
                return
            else:
                resp.status = falcon.HTTP_200

                resp.body = json.dumps(model_dict)

                return
        except:
            # Some sort of error occured, log
            resp.status = falcon.HTTP_500
            gcfg.logger.error("Some error occured, stack trace follows:")
            gcfg.logger.error(traceback.print_exc())
            return


###############################################################################
# Falcon service to retrieve model zip files                                  #
###############################################################################

def delete_zip_hook(req, resp, resource):
    gcfg = get_global_variables()
    config = read_config_from_environ()

    gcfg.logger.debug("req.path: {}".format(req.path))

    url_split = req.path.split('/')
    unique_id = url_split[-1]

    gcfg.logger.debug("unique_id: {}".format(unique_id))

    model_tmp_zip = os.path.join(
        config["file_root"], "temporary",
        "{}.zip".format(unique_id)
    )

    try:
        os.remove(model_tmp_zip)
    except FileNotFoundError:
        # If the file could not be found ignore.
        gcfg.logger.debug("Could not delete file {} since it doesn't exist!".format(model_tmp_zip))


class GetModelZipByUniqueId(M4DBService):
    r"""
    A class to encapsulate a Falcon REST service that will retrieve
    :class:`~m4db.orm.Model` objects in a zip from from m4db. The resulting
    zip file contains all data including geometry.
    """

    def __init__(self, access_level=ACCESS_READ):
        super().__init__(access_level)

    @falcon.after(delete_zip_hook)
    def on_get(self, req, resp, unique_id_zip):
        r"""
        Retrieve the information associated with a Model object that has
        the unique id given
        """
        self.check_access_level(req, resp)

        gcfg = get_global_variables()

        unique_id = os.path.splitext(unique_id_zip)[0]  # Because we drop the '.zip' suffix

        model_dict = model_by_unique_id(self.session, unique_id)
        self.session.commit()

        try:
            if model_dict is None:
                resp.status = falcon.HTTP_404
                return
            else:
                zip_file = create_model_zip(model_dict)

                resp.status = falcon.HTTP_200
                resp.content_type = mimetypes.guess_type(zip_file)[0]
                with open(zip_file, 'rb') as fout:
                    resp.body = fout.read()
                    fout.close()

                return
        except:
            # Some sort of error occured, log
            gcfg.logger.error("Some error occurred, stack trace follows:")
            gcfg.logger.error(traceback.print_exc())
            resp.status = falcon.HTTP_500
            return


###############################################################################
# Falcon service to retrieve model metadata given a set of parameters         #
###############################################################################

class GetModelsQuantsByMGST(M4DBService):
    r"""
    A class to encapsulate a Falcon REST sercvie to retrieve model metadata
    by mgst-tuple (material, geometry, size, temperature)
    """

    def __init__(self, access_level=ACCESS_READ):
        super().__init__(access_level)

    def on_get(self, req, resp, run_stat, user_name, proj_name, geometry, size, size_unit, size_conv):
        r"""
        Retrieve model metadata with the specified parameters.

        :param req:       the Falcon request object (containing the http post
                          data sent by the user.
        :param resp:      the Falcon response object.
        :param run_stat:  the m4db running status of the model 'e.g. finished'
        :param user_name: the m4db user name that the models should belong to
        :param proj_name: the m4db project that the models should belong to
        :param geometry:  the geometry name that the models should belong to
        :param size:      the geometry size
        :param size_unit: the size unit symbol (e.g. 'm', 'mm', 'cm' etc.)
        :param size_conv: the size convention ('esvd' or 'ecvl')
        """
        self.check_access_level(req, resp)

        gcfg = get_global_variables()

        try:
            materials, temperatures = process_materials_temperatures_from_http_get_parameters(req.params)

            models_quants = model_quants_by_mgst(
                session=self.session,
                run_status=run_stat,
                user_name=user_name,
                project_name=proj_name,
                materials=materials,
                geometry=geometry,
                size=size,
                size_unit=size_unit,
                size_convention=size_conv,
                temperatures=temperatures
            )
            self.session.commit()

            if models_quants == []:
                # The models were empty, respond to the user
                gcfg.logger.debug("The models were empty/no models found")
                resp.status = falcon.HTTP_404
                return
            else:
                # The models were not empty so send them back to the user
                gcfg.logger.debug("Sending something back to the user")
                resp.status = falcon.HTTP_200

                resp.body = json.dumps(models_quants)
                return

        except ValueError:
            # A value error occured, tell the user that the request was malformed
            # Some sort of error occurred, send a 500 to the user and log
            gcfg.logger.error("Malformed request")
            gcfg.logger.error(traceback.print_exc())
            resp.status = falcon.HTTP_401
            return
        except Exception:
            # Some sort of error occurred, send a 500 to the user and log
            gcfg.logger.error("Some error occured, stack trace follows:")
            gcfg.logger.error(traceback.print_exc())
            resp.status = falcon.HTTP_500
            return


class GetModelsByMGST(M4DBService):
    r"""
    A class to encapsulate a Falcon REST service to retrieve model metadata
    by mgst-tuple (material, geometry, size, temperature)
    """

    def __init__(self, access_level=ACCESS_READ):
        super().__init__(access_level)

    def on_get(self, req, resp, run_stat, user_name, proj_name, geometry, size, size_unit, size_conv):
        r"""
        Retrieve model metadata with the specified parameters.

        :param req:       the Falcon request object (containing the http post
                          data sent by the user.
        :param resp:      the Falcon response object.
        :param run_stat:  the m4db running status of the model 'e.g. finished'
        :param user_name: the m4db user name that the models should belong to
        :param proj_name: the m4db project that the models should belong to
        :param geometry:  the geometry name that the models should belong to
        :param size:      the geometry size
        :param size_unit: the size unit symbol (e.g. 'm', 'mm', 'cm' etc.)
        :param size_conv: the size convention ('esvd' or 'ecvl')
        """
        self.check_access_level(req, resp)

        gcfg = get_global_variables()

        try:
            materials, temperatures = process_materials_temperatures_from_http_get_parameters(req.params)

            models_dict = models_by_mgst(
                session=self.session,
                run_status=run_stat,
                user_name=user_name,
                project_name=proj_name,
                materials=materials,
                geometry=geometry,
                size=size,
                size_unit=size_unit,
                size_convention=size_conv,
                temperatures=temperatures
            )
            self.session.commit()

            if not models_dict:
                # The models were empty, respond to the user
                gcfg.logger.debug("The models were empty/no models found")
                resp.status = falcon.HTTP_404
                return
            else:
                # The models were not empty so send them back to the user
                gcfg.logger.debug("Sending something back to the user")
                resp.status = falcon.HTTP_200

                resp.body = json.dumps(models_dict)

                return
        except ValueError:
            # A value error occurred, tell the user that the request was malformed
            # Some sort of error occurred, send a 500 to the user and log
            gcfg.logger.error("Malformed request")
            gcfg.logger.error(traceback.print_exc())
            resp.status = falcon.HTTP_401
            return
        except Exception:
            # Some sort of error occurred, send a 500 to the user and log
            gcfg.logger.error("Some error occured, stack trace follows:")
            gcfg.logger.error(traceback.print_exc())
            resp.status = falcon.HTTP_500
            return


class GetModelsUniqueIdsByMGST(M4DBService):
    r"""
    A class to encapsulate a Falcon REST service to retrieve model metadata
    by mgst-tuple (material, geometry, size, temperature)
    """

    def __init__(self, access_level=ACCESS_READ):
        super().__init__(access_level)

    def on_get(self, req, resp, run_stat, user_name, proj_name, geometry, size, size_unit, size_conv):
        r"""
        Retrieve model metadata with the specified parameters.

        :param req:       the Falcon request object (containing the http post
                          data sent by the user.
        :param resp:      the Falcon response object.
        :param run_stat:  the m4db running status of the model 'e.g. finished'
        :param user_name: the m4db user name that the models should belong to
        :param proj_name: the m4db project that the models should belong to
        :param geometry:  the geometry name that the models should belong to
        :param size:      the geometry size
        :param size_unit: the size unit symbol (e.g. 'm', 'mm', 'cm' etc.)
        :param size_conv: the size convention ('esvd' or 'ecvl')
        """
        self.check_access_level(req, resp)

        gcfg = get_global_variables()

        try:
            materials, temperatures = process_materials_temperatures_from_http_get_parameters(req.params)

            unique_ids = models_unique_ids_by_mgst(
                session=self.session,
                run_status=run_stat,
                user_name=user_name,
                project_name=proj_name,
                materials=materials,
                geometry=geometry,
                size=size,
                size_unit=size_unit,
                size_convention=size_conv,
                temperatures=temperatures
            )
            self.session.commit()

            if unique_ids == []:
                # The models were empty, respond to the user
                gcfg.logger.debug("The models were empty/no models found")
                resp.status = falcon.HTTP_404
                return
            else:
                # The model unique ids were not empty so send them back to the user
                gcfg.logger.debug("Sending something back to the user")
                resp.status = falcon.HTTP_200

                resp.body = json.dumps({"unique_ids": unique_ids})
                return

        except ValueError:
            # A value error occurred, tell the user that the request was malformed
            # Some sort of error occurred, send a 500 to the user and log
            gcfg.logger.error("Malformed request")
            gcfg.logger.error(traceback.print_exc())
            resp.status = falcon.HTTP_401
            return
        except Exception:
            # Some sort of error occurred, send a 500 to the user and log
            gcfg.logger.error("Some error occured, stack trace follows:")
            gcfg.logger.error(traceback.print_exc())
            resp.status = falcon.HTTP_500
            return


###############################################################################
# Falcon service to retrieve model quants metadata given a set of parameters  #
###############################################################################

class GetModelQuantsByUniqueId(M4DBService):
    r"""
    A class to retrieve Model quants by unique id.
    """
    def __init__(self, access_level=ACCESS_READ):
        super().__init__(access_level)

    def on_get(self, req, resp, unique_id):
        self.check_access_level(req, resp)

        gcfg = get_global_variables()

        try:
            # Look for a model by unique id, using SQL Alchemy query.
            model_query = self.session.query(
                Model.mx_tot,
                Model.my_tot,
                Model.mz_tot,
                Model.vx_tot,
                Model.vy_tot,
                Model.vz_tot,
                Model.h_tot,
                Model.adm_tot,
                Model.e_typical,
                Model.e_anis,
                Model.e_ext,
                Model.e_demag,
                Model.e_exch1,
                Model.e_exch2,
                Model.e_exch3,
                Model.e_exch4,
                Model.e_tot
            ).filter(Model.unique_id == unique_id)

            (
                mx_tot,
                my_tot,
                mz_tot,
                vx_tot,
                vy_tot,
                vz_tot,
                h_tot,
                adm_tot,
                e_typical,
                e_anis,
                e_ext,
                e_demag,
                e_exch1,
                e_exch2,
                e_exch3,
                e_exch4,
                e_tot
            ) = model_query.one()

            model_quants = {
                "unique_id": unique_id,
                'mx_tot': mx_tot,
                'my_tot': my_tot,
                'mz_tot': mz_tot,
                'vx_tot': vx_tot,
                'vy_tot': vy_tot,
                'vz_tot': vz_tot,
                'h_tot': h_tot,
                'adm_tot': adm_tot,
                'e_typical': e_typical,
                'e_anis': e_anis,
                'e_ext': e_ext,
                'e_demag': e_demag,
                'e_exch1': e_exch1,
                'e_exch2': e_exch2,
                'e_exch3': e_exch3,
                'e_exch4': e_exch4,
                'e_tot': e_tot
            }

            resp.status = falcon.HTTP_200
            resp.body = json.dumps(model_quants)
        except NoResultFound as e:
            gcfg.logger.debug("No results found for unique id: '{}'".format(unique_id))
            resp.status = falcon.HTTP_404
            return
        except MultipleResultsFound as e:
            gcfg.logger.debug("Multiple results found for unique id: '{}'".format(unique_id))
            resp.status = falcon.HTTP_500
            return
        except:
            # Some sort of error occurred, log
            gcfg.logger.error("Some error occured, stack trace follows:")
            gcfg.logger.error(traceback.print_exc())
            resp.status = falcon.HTTP_500
            return


class NumpyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)


class GetModelMagnetizationByUniqueId(M4DBService):
    def __init__(self, access_level=ACCESS_READ):
        super().__init__(access_level)

    def on_get(self, req, resp, unique_id):
        r"""
        Retrieve the information associated with a Model object that has
        the unique id given
        """
        self.check_access_level(req, resp)

        config = read_config_from_environ()
        gcfg = get_global_variables()

        model = self.session.query(Model). \
            filter(Model.unique_id == unique_id). \
            one_or_none()

        try:
            if model is None:
                resp.status = falcon.HTTP_404
                return
            else:
                magnetization_tec = os.path.join(
                    config["file_root"], "model", uid_to_dir(model.unique_id), "magnetization.tec"
                )
                resp.body = json.dumps(
                    read_tecplot_file(magnetization_tec),
                    cls=NumpyEncoder
                )
                resp.status = falcon.HTTP_200
                return
        except:
            # Some sort of error occured, log
            gcfg.logger.error("Some error occurred, stack trace follows:")
            gcfg.logger.error(traceback.print_exc())
            resp.status = falcon.HTTP_500
            return


###############################################################################
# Falcon service to retrieve model summary data                               #
###############################################################################


class GetModelsSummaryByUserProject(M4DBService):
    r"""
    A class to encapsulate a Falcon REST service to query the database for
    some useful summary data
    """

    def __init__(self, access_level=ACCESS_READ):
        super().__init__(access_level)

    def on_get(self, req, resp, user_name, proj_name):
        r"""
        Retrieve summary data from the database

        :param req:       the Falcon request object (containing the http post data
                          sent by the user.
        :param resp:      the Falcon response object.
        :param user_name: the m4db user name that the models should belong to
        :param proj_name: the m4db project that the models should belong to
        """
        self.check_access_level(req, resp)

        gcfg = get_global_variables()

        try:
            summary_query = self.session.query(
                ModelMaterialsText.materials,
                Geometry.name,
                Geometry.size,
                Unit.symbol,
                func.count(text("*"))
            ).join(Model, Model.model_materials_text_id == ModelMaterialsText.id)\
             .join(Geometry, Model.geometry_id == Geometry.id)\
             .join(Metadata, Model.mdata_id == Metadata.id)\
             .join(DBUser, DBUser.id == Metadata.db_user_id)\
             .join(Project, Project.id == Metadata.project_id)\
             .join(Unit, Geometry.size_unit_id == Unit.id)\
            .filter(DBUser.user_name == user_name)\
            .filter(Project.name == proj_name)\
            .group_by(
                ModelMaterialsText.materials,
                Geometry.name,
                Geometry.size,
                Unit.symbol
            )

            summary = summary_query.all()

            # construct list of summary data records
            summary_data = []
            for materials, geometry, geometry_size, geometry_size_unit, count in summary:
                summary_data.append({
                    'materials': materials,
                    'geometry:': geometry,
                    'size': str(geometry_size),
                    'size_unit': geometry_size_unit,
                    'count': count
                })
            if summary_data == []:
                # The models were empty, respond to the user
                gcfg.logger.debug("The result set was empty")
                resp.status = falcon.HTTP_404
                return
            else:
                # The models were not empty so send them back to the user
                gcfg.logger.debug("Sending back results")
                resp.status = falcon.HTTP_200
                resp.body = json.dumps(
                    summary_data
                )
                return
        except:
            # Some sort of error occured, log
            gcfg.logger.error("Some error occured, stack trace follows:")
            gcfg.logger.error(traceback.print_exc())
            resp.status = falcon.HTTP_500
            return


# ###############################################################################
# # Falcon service to check a model and make sure it's valid                    #
# ###############################################################################
# class ValidateModel(M4DBService):
#     def __init__(self, access_level=ACCESS_WRITE):
#         super().__init__(access_level)
#
#     def on_get(self, req, resp, unique_id):
#         self.check_access_level(req, resp)
#         gcfg = get_global_variables()
#
#         # Verify that the model exists.
#         if not check_model_exists(self.session, unique_id):
#             resp.status = falcon.HTTP_404
#             return
#
#         # Construct the path in which the model data lives.
#         data_path = os.path.join(
#             gcfg.model_dir,
#             uid_to_dir(unique_id)
#         )
#
#         # Check that the path exists, if it doesnt then that's VERY bad!!

###############################################################################
###############################################################################
#          MODEL POST SERVICES (ones that change the DB) GO HERE              #
###############################################################################
###############################################################################

###############################################################################
# Falcon service to create a new model                                        #
###############################################################################

class AddNotRunModel(M4DBService):
    r"""
    A class to encapsulate a falcon REST service to create a new model.
    """
    def __init__(self, access_level=ACCESS_WRITE):
        super().__init__(access_level)

    def on_post(self, req, resp):
        r"""
        Add a brand new model to the database. Post data can be of the form.

        Eg 1                           |  Eg 2                            | Eg 3
       --------------------------------+----------------------------------+---------------------------------------
        {                              |  {                               |  {
          "user_name": "lnagy2",       |    "user_name": "lnagy2",        |    "user_name": "lnagy2",
          "project_name": "elongs",    |    "project_name": "elongs",     |    "project_name": "elongs",
          "repetitions": 1,            |    "repetitions": 1,             |    "repetitions": 1,
          "max_energy_evaluations": 10,|    "max_energy_evaluations": 10, |    "max_energy_evaluations": 10,<-- optional
          "geometry": {                |    "geometry": {                 |    "geometry": {
            "name": "cubo000",         |      "name": "cubo000",          |      "name": "cubo000",
            "size": "200",             |      "size": 200,                |      "size": 200,
            "size_unit": "nm",         |      "size_unit": "nm",          |      "size_unit": "nm",
            "size_convention": "esvd", |      "size_convention": "esvd",  |      "size_convention": "esvd",
          },                           |    },                            |    },
          "materials": [               |    "materials": [                |    "materials": [
          {                            |    {                             |    {
            "name": "iron",            |      "name": "iron",             |      "name": "iron",
            "temperature": 20,         |      "temperature": 20,          |      "temperature": 20,
            "temperature_unit": "C",   |      "temperature_unit": "C",    |      "temperature_unit": "C",
            "submesh_id": 1            |      "submesh_id": 1             |      "submesh_id": 1
          },                           |     }                            |    }
          {                            |     ],                           |    ],
            "name": "magnetite":       |     "start_magnetization": {     |    "start_magnetization": {
            "temperature": 20,         |       "type": "uniform",         |        "type": "model",
            "temperature_unit": "C",   |       "dir_x": 1.0,              |        "unique_id": "<uid_string>"
            "submesh_id": 2            |       "dir_y": 1.0,              |    },
          }                            |       "dir_z": 1.0,              |    "external_field": none
          ],                           |     },                           |  }
          "start_magnetization": {     |     "external_field": {          |
            "type": "random",          |       "dir_x": 1.0,              |
          },                           |       "dir_y": 1.0,              |
          "external_field": none       |       "dir_z": 1.0,              |
        }                              |       "magnitude": 30,           |
                                       |       "unit": "mT"               |
                                       |     }                            |
                                       |  }                               |
                                       |                                  |

        :param req:
        :param resp:
        :return:
        """
        self.check_access_level(req, resp)
        gcfg = get_global_variables()
        data = req.media

        ##################################################################################################
        # Basic request validation
        ##################################################################################################

        # Check that there is a 'project_name' part
        if "project_name" not in data.keys():
            gcfg.logger.debug("Request is missing 'project_name'")
            resp.status = falcon.HTTP_400
            return

        # Check that there is a 'repetitions' part
        if "repetitions" not in data.keys():
            gcfg.logger.debug("Request is missing 'repetitions'")
            resp.status = falcon.HTTP_400
            return

        # Check that there is a 'geometry' part.
        if "geometry" in data.keys():
            if "name" not in data["geometry"].keys():
                gcfg.logger.debug("Request is missing 'name' in 'geometry'")
                resp.status = falcon.HTTP_400
                return
            if "size" not in data["geometry"].keys():
                gcfg.logger.debug("Request is missing 'size' in 'geometry'")
                resp.status = falcon.HTTP_400
                return
            if "size_unit" not in data["geometry"].keys():
                gcfg.logger.debug("Request is missing 'size_unit' in 'geometry'")
                resp.status = falcon.HTTP_400
                return
            if "size_convention" not in data["geometry"].keys():
                gcfg.logger.debug("Request is missing 'size_convetion' in 'geometry'")
                resp.status = falcon.HTTP_400
                return
        else:
            gcfg.logger.debug("Request is missing 'geometry'")
            resp.status = falcon.HTTP_400
            return

        # Check that there is a 'materials' part
        if "materials" in data.keys():
            # Check each material
            for material in data["materials"]:
                if "name" not in material.keys():
                    gcfg.logger.debug("Request is missing 'name' from a material in 'materials'")
                    resp.status = falcon.HTTP_400
                    return
                if "temperature" not in material.keys():
                    gcfg.logger.debug("Request is missing 'temperature' from a material in 'materials'")
                    resp.status = falcon.HTTP_400
                    return
                if "temperature_unit" not in material.keys():
                    gcfg.logger.debug("Request is missing 'temperature_unit' from a material in 'materials'")
                    resp.status = falcon.HTTP_400
                    return
                if "submesh_id" not in material.keys():
                    gcfg.logger.debug("request is missing 'submesh_id' from material in 'materials'")
                    resp.status = falcon.HTTP_400
                    return
        else:
            gcfg.logger.debug("Request is missing 'materials'")
            resp.status = falcon.HTTP_400
            return

        # Check that there is an initial field
        FIELD_RANDOM = 1
        FIELD_UNIFORM = 2
        FIELD_MODEL = 3
        FIELD_NONE = 4

        if "start_magnetization" in data.keys():
            if "type" not in data["start_magnetization"]:
                gcfg.logger.debug("request is missing 'type' from 'start_magnetization'")
                resp.status = falcon.HTTP_400
                return
            else:
                if data["start_magnetization"]["type"] == "random":
                    start_magnetization_type = FIELD_RANDOM
                elif data["start_magnetization"]["type"] == "uniform":
                    start_magnetization_type = FIELD_UNIFORM
                    if "dir_x" not in data["start_magnetization"].keys():
                        gcfg.logger.debug("Request is missing 'dir_x' from uniform 'start_magnetization'")
                        resp.status = falcon.HTTP_400
                        return
                    if "dir_y" not in data["start_magnetization"].keys():
                        gcfg.logger.debug("Request is missing 'dir_y' from uniform 'start_magnetization'")
                        resp.status = falcon.HTTP_400
                        return
                    if "dir_z" not in data["start_magnetization"].keys():
                        gcfg.logger.debug("Request is missing 'dir_z' from uniform 'start_magnetization")
                        resp.status = falcon.HTTP_400
                        return
                elif data["start_magnetization"]["type"] == "model":
                    start_magnetization_type = FIELD_MODEL
                    if "unique_id" not in data["start_magnetization"].keys():
                        gcfg.logger.debug("Request is missing 'unique_id' model 'start_magnetization'")
                        resp.status = falcon.HTTP_400
                        return
                else:
                    gcfg.logger.debug(
                        "Unknown value '{}' for 'start_magnetization' 'type'".format(data["start_magnetization"]["type"])
                    )
                    resp.status = falcon.HTTP_400
                    return
        else:
            gcfg.logger.debug("Request is missing 'start_magnetization'")
            resp.status = falcon.HTTP_400
            return

        # Check that there is an external field
        external_field_type = None
        if "external_field" in data.keys():
            if data["external_field"] is None:
                external_field_type = FIELD_NONE
            else:
                external_field_type = FIELD_UNIFORM
                if "dir_x" not in data["external_field"].keys():
                    gcfg.logger.debug("Request is missing 'dir_x' for uniform external field")
                    resp.status = falcon.HTTP_400
                    return
                if "dir_y" not in data["external_field"].keys():
                    gcfg.logger.debug("Request is missing 'dir_y' for uniform external field")
                    resp.status = falcon.HTTP_400
                    return
                if "dir_z" not in data["external_field"].keys():
                    gcfg.logger.debug("Request is missing 'dir_z' for uniform external field")
                    resp.status = falcon.HTTP_400
                    return
                if "magnitude" not in data["external_field"].keys():
                    gcfg.logger.debug("Request is missing 'magnitude' for uniform external field")
                    resp.status = falcon.HTTP_400
                    return
                if "unit" not in data["external_field"].keys():
                    gcfg.logger.debug("Request is missing 'unit' for uniform external field")
                    resp.status = falcon.HTTP_400
                    return
        else:
            gcfg.logger.debug("Request is missing 'external_field'")
            resp.status = falcon.HTTP_400
            return

        # maximum energy evaluations (this defaults to 10000)
        if data.get("max_energy_evaluations") is not None:
            max_energy_evaluations = data.get("max_energy_evaluations")
        else:
            max_energy_evaluations = 10000

        ##################################################################################################
        # Retrieve objects associated with the request
        ##################################################################################################

        try:
            db_user = self.session.query(DBUser).\
                filter(DBUser.user_name == req.get_header("user_name")).\
                one_or_none()
            if db_user is None:
                gcfg.logger.debug("Could not find user '{}' in the database".format(req.get_header("user_name")))
                resp.status = falcon.HTTP_400
                return

            project = self.session.query(Project).\
                filter(Project.name == data["project_name"]).\
                one_or_none()
            if project is None:
                gcfg.logger.debug("Could not find project '{}' in the database".format(data["project_name"]))
                resp.status = falcon.HTTP_400
                return

            size_in_micron = convert_to_micron(
                data["geometry"]["size"],
                data["geometry"]["size_unit"]
            )

            size_convention = data["geometry"]["size_convention"].upper()

            SizeUnit = aliased(Unit)
            geometry = self.session.query(Geometry).\
                join(SizeUnit, SizeUnit.id == Geometry.size_unit_id).\
                join(SizeConvention, SizeConvention.id == Geometry.size_convention_id).\
                filter(Geometry.name == data["geometry"]["name"]).\
                filter(Geometry.size == size_in_micron).\
                filter(SizeConvention.symbol == size_convention).\
                one_or_none()
            if geometry is None:
                gcfg.logger.debug("Could not find geometry")
                resp.status = falcon.HTTP_400
                return

            material_text = []
            submesh_id_material_text = []
            submesh_id_material_temperature_text = []
            materials = []
            for data_material in data["materials"]:

                material_text.append(data_material["name"])

                submesh_id_material_text.append("{}:{}".format(
                    data_material["submesh_id"], data_material["name"]
                ))


                submesh_id_material_temperature_text.append("{}:{}:{}".format(
                    data_material["submesh_id"],
                    data_material["name"],
                    "{:8.3f}".format(data_material["temperature"]).strip()
                ))

                material = self.session.query(Material).\
                    filter(Material.name == data_material["name"]).\
                    filter(Material.temperature == data_material["temperature"]).\
                    one_or_none()

                if material is None:
                    gcfg.logger.debug("Could not find material")
                    resp.status = falcon.HTTP_400
                    return
                else:
                    materials.append({
                        "submesh_id": data_material["submesh_id"],
                        "material": material
                    })

            # Initial field model (if needed)
            if start_magnetization_type == FIELD_MODEL:
                start_magnetization_model = self.session.query(Model).\
                    filter(Model.unique_id == data["start_magnetization"]["unique_id"]).\
                    one_or_none()
                if start_magnetization_model is None:
                    gcfg.logger.debug("Could not find initial field model: {}".format(
                        data["start_magnetization"]["unique_id"]
                    ))
                    resp.status = falcon.HTTP_400
                    return
            else:
                # If used, this will cause an exception
                start_magnetization_model = 0

            # Internal field unit is '1' (i.e unitless since it is a direction)
            unitless_unit = self.session.query(Unit).\
                filter(Unit.symbol == "1").\
                one()

            # External field unit (if needed)
            if external_field_type == FIELD_UNIFORM:
                external_field_unit = self.session.query(Unit).\
                    filter(Unit.symbol == data["external_field"]["unit"]).\
                    one_or_none()
                if external_field_unit is None:
                    gcfg.logger.debug("Uniform external field unit '{}' could not be found in database".format(
                        data["external_field"]["unit"]
                    ))
                    resp.status = falcon.HTTP_400
                    return
            else:
                # Will throw an exception if we attempt to use
                external_field_unit = 0

            unrun_running_status = self.session.query(RunningStatus).\
                filter(RunningStatus.name == "not-run").\
                one_or_none()
            if unrun_running_status is None:
                gcfg.logger.debug("Running status 'not-run' not found in database")
                resp.status = falcon.HTTP_400
                return

            # This is a list of the unique IDs of the new models.
            new_models = []

            # Depending on how many repetitions we've asked for, add a model for each rep.
            for rep in range(data["repetitions"]):
                # Create a new initial field for each model rep
                start_magnetization = None
                if start_magnetization_type == FIELD_RANDOM:
                    start_magnetization = RandomField()
                elif start_magnetization_type == FIELD_UNIFORM:
                    start_magnetization = UniformField(
                        dx=data["start_magnetization"]["dir_x"],
                        dy=data["start_magnetization"]["dir_y"],
                        dz=data["start_magnetization"]["dir_z"],
                        magnitude=1.0,
                        unit=unitless_unit
                    )
                elif start_magnetization_type == FIELD_MODEL:
                    start_magnetization = ModelField(
                        model=start_magnetization_model
                    )

                # Create a new external field for each model rep
                external_field = None
                if external_field_type == FIELD_NONE:
                    external_field = None
                elif external_field_type == FIELD_UNIFORM:
                    external_field = UniformField(
                        dx=data["external_field"]["dir_x"],
                        dy=data["external_field"]["dir_y"],
                        dz=data["external_field"]["dir_z"],
                        magnitude=data["external_field"]["magnitude"],
                        unit=external_field_unit
                    )

                model = Model(
                    max_energy_evaluations=max_energy_evaluations,
                    geometry=geometry,
                    start_magnetization=start_magnetization,
                    external_field=external_field,
                    running_status=unrun_running_status,
                    model_run_data=ModelRunData(),
                    model_report_data=ModelReportData(),
                    mdata=Metadata(
                        db_user=db_user,
                        project=project
                    ),
                    model_materials_text=ModelMaterialsText(
                        materials=",".join(material_text),
                        submeshidxs_materials=",".join(submesh_id_material_text),
                        submeshidxs_materials_temperatures=",".join(submesh_id_material_temperature_text)
                    )
                )
                for mat_idx in materials:
                    mma = ModelMaterialAssociation(
                        material=mat_idx["material"],
                        submesh_id=mat_idx["submesh_id"]
                    )
                    model.materials.append(mma)
                self.session.add(model)
                new_models.append(model)
            self.session.commit()

            resp.status = falcon.HTTP_200
            resp.body = json.dumps({"unique_ids": [model.unique_id for model in new_models]})
        except:
            # Some sort of error occured, log
            gcfg.logger.error("Some error occured, stack trace follows:")
            gcfg.logger.error(traceback.print_exc())
            resp.status = falcon.HTTP_500
            return

###############################################################################
# Falcon service to set model running statuses                                #
###############################################################################


class SetModelRunningStatus(M4DBService):
    r"""
    A class to encapsulate a Falcon REST service to change the running status
    of a :class:`~m4db.orm.Model` object, i.e. there are not checks as to what
    the existing model's running status is, the running status is just changed.
    """

    def __init__(self, access_level=ACCESS_ADMIN):
        super().__init__(access_level)

    def on_post(self, req, resp):
        """
        Update a model's `running_status` using http post data. This data
        is in the form of a JSON string and should have the same form as
        outlined in :class:`~m4db.rest.message.model.RunningStatusNew`.

        :param req:  the Falcon request object (containing the http post data
                     sent by the user, this is a peice of JSON that looks like
                     {
                        "unique_id": "<the_unique_id>",
                        "new_status": "<the new status>"
                     }
        :param resp: the Falcon response object.
        """
        self.check_access_level(req, resp)

        gcfg = get_global_variables()

        data = req.media

        # Check message

        unique_id = data.get("unique_id")
        new_status = data.get("new_running_status")

        if unique_id is None:
            gcfg.logger.debug("Parameter 'unique_id' was missing from request data")
            resp.status = falcon.HTTP_400
            return

        if new_status is None:
            gcfg.logger.debug("Parameter 'new_status' was missing from request data")
            resp.status = falcon.HTTP_400
            return

        # Perform update

        try:
            # Get the running status id of the new running status
            # *WARNING* notice the comma after the variable name, this is because this query returns a tuple of
            #           once value and not a value!!!
            running_status_id, = self.session.query(RunningStatus.id). \
                filter(RunningStatus.name == new_status). \
                one_or_none()

            gcfg.logger.debug("Setting to new running status id: {}".format(running_status_id))

            if running_status_id is None:
                # Malformed request, user data failed validation
                gcfg.logger.debug("Could not find running status name '{}' in database".format(new_status))
                resp.status = falcon.HTTP_400
                return

            # Set the model's running_status_id
            stmt = update(Model).where(Model.unique_id == unique_id).values(running_status_id=running_status_id)
            res = self.session.execute(stmt)
            gcfg.logger.debug("Updated running status, no. of models updated: {}".format(res.rowcount))

            # Make a check on the number of updated rows.
            if res.rowcount == 0:
                gcfg.logger.debug("No models were updated!")
                self.session.rollback()
                resp.status = falcon.HTTP_404
                return
            elif res.rowcount > 1:
                gcfg.logger.debug("Attempted to update multiple models with unique id: '{}'".format(unique_id))
                self.session.rollback()
                resp.status = falcon.HTTP_500
                return
            else:
                gcfg.logger.debug("Committing changes")
                self.session.commit()
                resp.status = falcon.HTTP_200
                return
        except:
            # Some sort of error occured, log
            gcfg.logger.error("Some error occured, stack trace follows:")
            gcfg.logger.error(traceback.print_exc())
            resp.status = falcon.HTTP_500
            return


###############################################################################
# Falcon service to set model quantities                                      #
###############################################################################


class SetModelQuants(M4DBService):
    r"""
    A class to encapsulate a Falcon REST service to set Model quantities.
    """

    def __init__(self, access_level=ACCESS_ADMIN):
        super().__init__(access_level)

    def on_post(self, req, resp):
        r"""
        Update model quantities using http post data. This data is in the form
        of a JSON string and should have the same form as outlined in
        :class:`~m4db.rest.message.model.ModelQuants`

        :param req:  the Falcon request object (containing the http post data
                     sent by the user.
        :param resp: the Falcon response object.

        Note: response returns http errors on following condition
              * 400 - the data being sent to the service was invalid/missing values
              * 404 - the model to update was not found
              * 500 - some other error, check logs
        """
        self.check_access_level(req, resp)

        gcfg = get_global_variables()

        data = req.media

        # Check message

        unique_id = data.get("unique_id")

        if unique_id is None:
            gcfg.logger.debug("Parameter 'unique_id' was missing from request data")
            resp.status = falcon.HTTP_400
            return

        # Create a list of field names can be updated
        quant_fields = [
            "mx_tot",
            "my_tot",
            "mz_tot",
            "vx_tot",
            "vy_tot",
            "vz_tot",
            "h_tot",
            "adm_tot",
            "e_typical",
            "e_anis",
            "e_ext",
            "e_demag",
            "e_exch1",
            "e_exch2",
            "e_exch3",
            "e_exch4",
            "e_tot"
        ]

        try:
            update_values = {}
            for fn in quant_fields:
                if data.get(fn) is not None:
                    update_values[fn] = data.get(fn)

            # Update the model's quants
            stmt = update(Model).where(Model.unique_id == unique_id).values(**update_values)
            res = self.session.execute(stmt)
            gcfg.logger.debug("Updated quants, no. of models updated: {}".format(res.rowcount))

            # Make a check on the number of updated rows.
            if res.rowcount == 0:
                gcfg.logger.debug("No models were updated!")
                self.session.rollback()
                resp.status = falcon.HTTP_404
                return
            elif res.rowcount > 1:
                gcfg.logger.debug("Attempted to update multiple models with unique id: '{}'".format(unique_id))
                self.session.rollback()
                resp.status = falcon.HTTP_500
                return
            else:
                gcfg.logger.debug("Committing changes")
                self.session.commit()
                resp.status = falcon.HTTP_200
                return

        except:
            # Some sort of error occured, log
            gcfg.logger.error("Some error occured, stack trace follows:")
            gcfg.logger.error(traceback.print_exc())
            resp.status = falcon.HTTP_500
            return
