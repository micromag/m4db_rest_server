import falcon

from datetime import datetime

from sqlalchemy import and_

from m4db_rest_server.global_config import get_global_variables

from m4db_database.sessions import get_session

from m4db_database.utilities import password_hash

from m4db_database.orm import DBUser


class SQLAlchemySessionManager:
    """
    Create a scoped session for every request and close it when the request
    ends.
    """

    def __init__(self, Session):
        self.Session = Session

    def process_resource(self, req, resp, resource, params):
        resource.session = self.Session()

    def process_response(self, req, resp, resource, req_succeeded):
        if hasattr(resource, 'session'):
            self.Session.remove()

# A list of services that do not need authorization are listed here.
UNAUTHORIZED_SERVICES = [
    "/user/forgotten",
    "/alive",
    "/running-status/all"
]

# A list of services that are authenticated using the user's username/password are listed here.
PASSWORD_AUTHORIZED_SERVICES = [
    "/user/ticket-hash"
]

class Authorization:
    r"""
    This is the authorization middleware module. Services on the UNAUTHORIZED_SERVICES list are open to anyone,
    services on the PASSWORD_AUTHORIZED_SERVICES list are authorized using the user's user name & password combination
    and all other services are authorized using the user's user name and hash ticket.
    """
    def process_request(self, req, resp):
        r"""
        Before routing has taken place, make sure that the user can be authenticated and set the access level that
        the user may have access to.
        :param req: falcon request object
        :param resp: falcon response object
        :return: nothing
        """
        gcfg = get_global_variables()
        gcfg.logger.debug("req.path: '{}'".format(req.path))
        if req.path in UNAUTHORIZED_SERVICES:
            self.unauthorized(req, resp)
        elif req.path in PASSWORD_AUTHORIZED_SERVICES:
            self.authorize_by_password(req, resp)
        else:
            self.authorize_by_ticket_hash(req, resp)
        gcfg.logger.debug("Authorization complete ... passing over to the service object")

    def unauthorized(self, req, resp):
        gcfg = get_global_variables()
        gcfg.logger.debug("unauthorized() has been called.")

        gcfg.logger.debug(
            "Adding users access level to request context (in this case set to zero without hitting the database)."
        )
        req.context.user_access_level = 0

    def authorize_by_password(self, req, resp):
        gcfg = get_global_variables()
        gcfg.logger.debug("authorized_by_password() has been called.")

        session = get_session()

        try:
            user_name = req.get_header("user_name")
            user_password = req.get_header("password")

            gcfg.logger.debug("user_name: {}".format(user_name))
            gcfg.logger.debug("password: {}".format(user_password))

            # If either user_name or password is not supplied, raise an error.
            if user_name is None or user_password is None:
                description = "User/password not supplied in header"
                gcfg.logger.debug(description)
                raise falcon.HTTPUnauthorized("User password authentication error", description)

            # Hash the password.
            pwd_hash = password_hash(user_password)
            gcfg.logger.debug("pwd_hash: '{}'".format(pwd_hash))

            # Retrieve the user from the database.
            db_user = session.query(DBUser).\
                filter(DBUser.user_name == user_name).\
                filter(DBUser.password == pwd_hash).\
                one_or_none()

            # If no such user if found, then error.
            if db_user is None:
                description = "Database user/password not found."
                gcfg.logger.debug(description)
                raise falcon.HTTPUnauthorized("User password authentication error", description)

            # If the user is found, add the access_level to the the request context
            gcfg.logger.debug("Adding users access level to request context.")
            req.context.user_access_level = db_user.access_level
        finally:
            gcfg.logger.debug("authorize_by_password(), cleaning up session")
            session.close()

    def authorize_by_ticket_hash(self, req, resp):
        gcfg = get_global_variables()
        gcfg.logger.debug("authorized_by_ticket() has been called.")

        session = get_session()

        try:
            user_name = req.get_header("user_name")
            ticket_hash = req.get_header("ticket_hash")

            if user_name is None or ticket_hash is None:
                description = "User/ticket hash not supplied in header"
                gcfg.logger.debug(description)
                raise falcon.HTTPUnauthorized("User ticket hash authentication error", description)

            # Retrieve the user
            db_user = session.query(DBUser).\
                filter(DBUser.user_name == user_name).\
                filter(and_(DBUser.ticket_hash == ticket_hash, DBUser.ticket_hash != None)).\
                one_or_none()

            # If no such user if found, then error.
            if db_user is None:
                description = "Database user/ticket combination not found."
                gcfg.logger.debug(description)
                raise falcon.HTTPUnauthorized("User ticket hash authentication error", description)

            # If the ticket is out of date.
            if db_user.ticket_timeout <= datetime.now():
                description = "User's ticket has timed out."
                gcfg.logger.debug(description)
                raise falcon.HTTPUnauthorized("User ticket hash authentication error", description)

            # If the user is found, add the access level to the request context
            gcfg.logger.debug("Adding users access level to request context.")
            req.context.user_access_level = db_user.access_level
        finally:
            gcfg.logger.debug("authorize_by_ticket(), cleaning up session")
            session.close()
