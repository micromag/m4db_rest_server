r"""
The application layer.
"""

import falcon

from m4db_database.sessions import get_session

from m4db_rest_server.middleware import SQLAlchemySessionManager
from m4db_rest_server.middleware import Authorization

from m4db_rest_server.model import GetModelByUniqueId
from m4db_rest_server.model import GetModelZipByUniqueId
from m4db_rest_server.model import GetModelQuantsByUniqueId
from m4db_rest_server.model import GetModelsByMGST
from m4db_rest_server.model import GetModelsUniqueIdsByMGST
from m4db_rest_server.model import GetModelsQuantsByMGST
from m4db_rest_server.model import GetModelMagnetizationByUniqueId
from m4db_rest_server.model import GetModelsSummaryByUserProject
from m4db_rest_server.model import AddNotRunModel

from m4db_rest_server.model import SetModelQuants
from m4db_rest_server.model import SetModelRunningStatus


from m4db_rest_server.neb import GetNebByUniqueId
from m4db_rest_server.neb import GetNEBPathStructureEnergiesByUniqueId
from m4db_rest_server.neb import GetPathRootByStartEndUniqueIds

from m4db_rest_server.neb import SetNEBRunningStatus
from m4db_rest_server.neb import AddNotRunNEBPath

from m4db_rest_server.running_status import GetAllRunningStatuses

from m4db_rest_server.geometry import GetGeometries
from m4db_rest_server.geometry import GetGeometryByUniqueId
from m4db_rest_server.geometry import GetGeometryZipByUniqueId

from m4db_rest_server.alive import Alive

from m4db_rest_server.user import GetAllUsers
from m4db_rest_server.user import NewUser
from m4db_rest_server.user import ForgottenPassword
from m4db_rest_server.user import TicketHash
from m4db_rest_server.user import ChangePassword

from m4db_rest_server.software import GetAllSoftware
from m4db_rest_server.software import NewSoftware
from m4db_rest_server.software import GetSoftwareByModelUniqueId

from m4db_rest_server.project import GetProjectByName
from m4db_rest_server.project import GetAllProject
from m4db_rest_server.project import NewProject

Session = get_session(scoped=True, echo=True)

app = falcon.API(middleware=[
        SQLAlchemySessionManager(Session),
        Authorization()
])
app.req_options.auto_parse_form_urlencoded = True


get_model_by_unique_id = GetModelByUniqueId()
get_model_zip_by_unique_id = GetModelZipByUniqueId()
get_model_quants_by_unique_id = GetModelQuantsByUniqueId()
get_models_by_mgst = GetModelsByMGST()
get_models_unique_ids_by_mgst = GetModelsUniqueIdsByMGST()
get_models_quants_by_mgst = GetModelsQuantsByMGST()
get_model_magnetization_by_unique_id = GetModelMagnetizationByUniqueId()
get_models_summary_by_user_project = GetModelsSummaryByUserProject()

set_model_quants = SetModelQuants()
set_model_running_status = SetModelRunningStatus()
add_not_run_model = AddNotRunModel()

get_neb_by_unique_id = GetNebByUniqueId()
get_path_structure_energies_by_unique_id = GetNEBPathStructureEnergiesByUniqueId()
get_path_root_by_start_end_unique_ids = GetPathRootByStartEndUniqueIds()
set_neb_running_status = SetNEBRunningStatus()
add_not_run_neb_path = AddNotRunNEBPath()

get_all_running_statuses = GetAllRunningStatuses()

get_geometries = GetGeometries()
get_geometry_by_unique_id = GetGeometryByUniqueId()
get_geometry_zip_by_unique_id = GetGeometryZipByUniqueId()

alive = Alive()

get_all_users = GetAllUsers()
add_new_user = NewUser()
forgotten_password = ForgottenPassword()
ticket_hash = TicketHash()
change_password = ChangePassword()

get_all_softwares = GetAllSoftware()
get_softwares_by_model_unique_id = GetSoftwareByModelUniqueId()
add_new_software = NewSoftware()

get_project_by_name = GetProjectByName()
get_all_projects = GetAllProject()
add_new_project = NewProject()

#######################
# model.py
#######################

app.add_route(
        '/model/{unique_id}',
        get_model_by_unique_id
)

app.add_route(
        '/model/zip/{unique_id_zip}',
        get_model_zip_by_unique_id
)

app.add_route(
        '/model/quants/{unique_id}',
        get_model_quants_by_unique_id
)

app.add_route(
        '/model/magnetization/{unique_id}',
        get_model_magnetization_by_unique_id
)

app.add_route(
        '/models/quants/{run_stat}/{user_name}/{proj_name}/{geometry}/{size}/{size_unit}/{size_conv}',
        get_models_quants_by_mgst
)

app.add_route(
        '/models/{run_stat}/{user_name}/{proj_name}/{geometry}/{size}/{size_unit}/{size_conv}',
        get_models_by_mgst
)

app.add_route(
        '/models/unique-ids/{run_stat}/{user_name}/{proj_name}/{geometry}/{size}/{size_unit}/{size_conv}',
        get_models_unique_ids_by_mgst
)


app.add_route(
        '/models/summary/{user_name}/{proj_name}',
        get_models_summary_by_user_project
)

app.add_route(
        '/model/set-quants',
        set_model_quants
)

app.add_route(
        '/model/set-running-status',
        set_model_running_status
)

app.add_route(
        '/model/add/not-run',
        add_not_run_model
)

#######################
# neb.py
#######################

app.add_route(
        '/neb/{unique_id}',
        get_neb_by_unique_id
)

app.add_route(
        '/neb/path-structure-energies/{unique_id}',
        get_path_structure_energies_by_unique_id
)

app.add_route(
        '/neb/get-path-root-by-start-end-unique-ids/{unique_id_one}/{unique_id_two}',
        get_path_root_by_start_end_unique_ids
)

app.add_route(
        '/neb/set-running-status',
        set_neb_running_status
)

app.add_route(
        '/neb/add',
        add_not_run_neb_path
)

#######################
# running_status.py
#######################
app.add_route(
        '/running-status/all',
        get_all_running_statuses
)

#######################
# geometry.py
#######################

app.add_route(
        '/geometry/all',
        get_geometries
)
app.add_route(
        '/geometries/summary',
        get_geometries
)

app.add_route(
        '/geometry/{unique_id}',
        get_geometry_by_unique_id
)

app.add_route(
        '/geometry/zip/{unique_id_zip}',
        get_geometry_zip_by_unique_id
)


#######################
# alive.py
#######################

app.add_route(
        '/alive',
        alive
)

#######################
# user.py
#######################
app.add_route(
        '/user/all',
        get_all_users
)

app.add_route(
        '/user/new',
        add_new_user
)

app.add_route(
        "/user/forgotten",
        forgotten_password
)

app.add_route(
        "/user/ticket-hash",
        ticket_hash
)

app.add_route(
        "/user/change-password",
        change_password
)

#######################
# software.py
#######################

app.add_route(
        '/software/all',
        get_all_softwares
)

app.add_route(
        '/software/{unique_id}',
        get_softwares_by_model_unique_id
)

app.add_route(
        '/software/new',
        add_new_software
)

#######################
# project.py
#######################

app.add_route(
        '/project/{name}',
        get_project_by_name
)

app.add_route(
        '/project/all',
        get_all_projects
)

app.add_route(
        '/project/new',
        add_new_project
)
