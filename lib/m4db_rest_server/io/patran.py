def parse_patran_header(file_name):
    r"""
    Parse the header information in a patran file.

    Arguments:

        file_name : the (full path) name of the patran file

    Returns:
        A pair with the first value being the number of vertices and the second value
        being the number of elements.
    """
    regex_header = re.compile(r'26\s+0\s+0\s+1\s+(\d+)\s+(\d+)\s+1\s+1\s+0')

    with open(file_name, 'r') as fin:
        for line in fin:
            match_header = regex_header.match(line)
            if match_header:
                return int(match_header.group(1)), int(match_header.group(2))