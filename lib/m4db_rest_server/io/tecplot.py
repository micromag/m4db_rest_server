r"""
Reads tecplot files, this includes single/multiphase files.
"""
import re

from enum import Enum


class TecplotFiletype(Enum):
    SINGLE_PHASE = 1
    MULTI_PHASE = 2


def tecplot_file_type(filename):
    r"""
    Retrieves the file type of the input tecplot file name.
    :param filename: the file name of the tecplot file.
    :return: a TecplotFileType object.
    """
    regex_single_phase = re.compile(r"FEPOINT")
    regex_multi_phase = re.compile(r"CELLCENTERED")
    MAX_ITR = 10
    with open(filename, 'r') as fin:
        for i, line in enumerate(fin):
            if i < MAX_ITR:
                if regex_multi_phase.search(line):
                    return TecplotFiletype.MULTI_PHASE
                if regex_single_phase.search(line):
                    return TecplotFiletype.SINGLE_PHASE


def read_tecplot_file(filename):
    r"""
    Returns tecplot file datga based on the type of tecplot file, this information is read using
    the header information within the tecplot file.
    :param filename: the tecplot file that needs to be read.
    :return: tecplot file data
    """
    file_type = tecplot_file_type(filename)

    if file_type == TecplotFiletype.SINGLE_PHASE:
        from m4db_rest_server.io.tecplot_single_phase import read_tecplot_file
        return read_tecplot_file(filename)
    elif file_type == TecplotFiletype.MULTI_PHASE:
        from m4db_rest_server.io.tecplot_multiphase import read_tecplot_file
        return read_tecplot_file(filename)

