r"""
This is a set of routines that will read common file formats.
"""


import sys
import re

from argparse import ArgumentParser, RawTextHelpFormatter
from textwrap import dedent

import numpy as np


def read_tecplot_file(filename):

    regex_title = re.compile(
        r'^\s*TITLE\s*=(.*)$'
    )
    regex_variables = re.compile(
        r'^\s*VARIABLES\s*=\s*(.*)$'
    )
    regex_zone = re.compile(
        r'^\s*ZONE\s+T\s*=\s*"(.*)"\s*N\s*=\s*(\d+)\s*,\s*E\s*=\s*(\d+)\s*$'
    )
    regex_element_type = re.compile(
        r'^\s*F\s*=\s*(.*)\s*,\s*ET\s*=\s*(.*)\s*$'
    )
    regex_zone_and_element_type = re.compile(
        r'^\s*ZONE\s+T\s*=\s*"(.*)"\s*N\s*=\s*(\d+)\s*,\s*E\s*=\s*(\d+),?\s*F\s*=\s*(.*)\s*,\s*ET\s*=\s*(.*)\s*$'
    )

    READ_TITLE = 1
    READ_VARIABLES = 2
    READ_ZONE = 3
    READ_ELEMENT_TYPE = 4
    READ_FIRST_ZONE_DATA = 5
    READ_ELEMENT_DATA = 6

    title = None
    variables = None
    nvert = None
    nelem = None
    fields = []
    vertices = None
    elements = None

    state = None
    line_counter = 0

    zone_index = -1
    with open(filename, 'r') as fin:
        lines = fin.readlines()

    error_max = 10
    error_counter = 0
    while True:
        line = lines[line_counter].strip()

        error_counter = error_counter + 1

        if error_counter >= error_max:
            raise IOError("Unable to parse file")

        if state is None:
            re_match_title = regex_title.match(line)
            if re_match_title:
                # Reset the error counter
                error_counter = 0
                # Extract title
                title = re_match_title.group(1)
                # Set state indicating zone info is read
                state = READ_TITLE
                line_counter = line_counter + 1

        elif state == READ_TITLE:
            re_match_variables = regex_variables.match(line)
            if re_match_variables:
                # Reset the error counter
                error_counter = 0
                str_vars = re_match_variables.group(1)
                lst_vars = str_vars.split(',')
                variables = [v.replace('"', '').strip() for v in lst_vars]
                state = READ_VARIABLES
                line_counter = line_counter + 1

        elif state == READ_VARIABLES:
            re_match_zone = regex_zone.match(line)
            if re_match_zone:
                # Reset the error counter
                error_counter = 0
                # Extract no. of vertices/elements
                nvert = int(re_match_zone.group(2))
                nelem = int(re_match_zone.group(3))
                # Create vertices array
                vertices = np.zeros((nvert, 3), dtype=np.float64)
                # Create elements array
                elements = np.zeros((nelem, 4), dtype=np.uint64)
                # Add a field array to the list of fields
                fields.append(np.zeros((nvert, 3), dtype=np.float64))
                # Set state indicating zone info is read
                state = READ_ZONE
                line_counter = line_counter + 1

                continue
            re_match_zone_and_element_type = regex_zone_and_element_type.match(line)
            if re_match_zone_and_element_type:
                # Reset the error counter
                error_counter = 0
                # Extract no. of vertices/elements
                nvert = int(re_match_zone_and_element_type.group(2))
                nelem = int(re_match_zone_and_element_type.group(3))
                point_type = re_match_zone_and_element_type.group(4)
                element_type = re_match_zone_and_element_type.group(5)
                # Create vertices array
                vertices = np.zeros((nvert, 3), dtype=np.float64)
                # Create elements array
                elements = np.zeros((nelem, 4), dtype=np.uint64)
                # Add a field array to the list of fields
                fields.append(np.zeros((nvert, 3), dtype=np.float64))
                # Set state indicating zone info is read
                state = READ_ELEMENT_TYPE
                line_counter = line_counter + 1

                continue
        elif state == READ_ZONE:
            re_match_element_type = regex_element_type.match(line)
            if re_match_element_type:
                # Reset the error counter
                error_counter = 0
                point_type = re_match_element_type.group(1).strip()
                element_type = re_match_element_type.group(2).strip()
                state = READ_ELEMENT_TYPE
                line_counter = line_counter + 1

        elif state == READ_ELEMENT_TYPE:
            # Reset the error counter
            error_counter = 0
            # Increment zone_index
            zone_index = zone_index + 1
            # Set an index within the zone
            idx = 0
            # Process the block
            for vert_field_line in lines[line_counter:line_counter+nvert]:
                numbers = [float(v) for v in vert_field_line.split()]
                assert len(numbers) == 6
                vertices[idx][0] = numbers[0]
                vertices[idx][1] = numbers[1]
                vertices[idx][2] = numbers[2]
                fields[zone_index][idx][0] = numbers[3]
                fields[zone_index][idx][1] = numbers[4]
                fields[zone_index][idx][2] = numbers[5]
                idx = idx + 1
            state = READ_FIRST_ZONE_DATA
            line_counter = line_counter+nvert
        elif state == READ_FIRST_ZONE_DATA:
            # Reset the error counter
            error_counter = 0
            idx = 0
            for elem_line in lines[line_counter:line_counter+nelem]:
                numbers = [int(v) for v in elem_line.split()]
                assert len(numbers) == 4
                elements[idx][0] = numbers[0]-1
                elements[idx][1] = numbers[1]-1
                elements[idx][2] = numbers[2]-1
                elements[idx][3] = numbers[3]-1
                idx = idx + 1
            state = READ_ELEMENT_DATA
            line_counter = line_counter + nelem
            # If there are no more lines then exit
            if line_counter >= len(lines):
                break
        elif state == READ_ELEMENT_DATA:
            # Reset the error counter
            error_counter = 0
            # Skip the two lines ahead
            line_counter = line_counter + 2
            # Increment zone_index
            zone_index = zone_index + 1
            # Set an index within the zone
            idx = 0
            # Add a new field
            fields.append(np.zeros((nvert, 3), dtype=np.float64))
            # Process the lines
            for field_line in lines[line_counter:line_counter+nvert]:
                numbers = [float(v) for v in field_line.split()]
                assert len(numbers) == 3
                fields[zone_index][idx][0] = numbers[0]
                fields[zone_index][idx][1] = numbers[1]
                fields[zone_index][idx][2] = numbers[2]
                idx = idx + 1
            line_counter = line_counter + nvert
            # If there are no more lines then exit
            if line_counter >= len(lines):
                break

    return {
        'fields': fields,
        'vertices': vertices,
        'elements': elements,
        'nfields': len(fields),
        'nvert': nvert,
        'nelem': nelem
    }
