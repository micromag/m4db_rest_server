r"""
This module contains a selection of Falcon web services that revolve around geometries.
"""
import os
import json
import falcon
import traceback
import mimetypes

from m4db_rest_server.global_config import get_global_variables

from m4db_core.db.query.geometry.query_functions import geometry_unique_id_exists
from m4db_core.db.query.geometry.query_functions import geometry_by_unique_id
from m4db_core.db.query.geometry.query_functions import geometry_summary

from m4db_rest_server.utilities.geometry import create_geometry_zip

from m4db_database.configuration import read_config_from_environ

from m4db_database.orm import ACCESS_READ

from m4db_rest_server.services import M4DBService


class GetGeometryByUniqueId(M4DBService):
    r"""
    A class to encapsulate a Falcon REST service to retrieve Geometry objects from the database by unique id.
    """

    def __init__(self, access_level=ACCESS_READ):
        super().__init__(access_level)

    def on_get(self, req, resp, unique_id):
        r"""
        Retrieve the information associated with a Geometry object that has the unique id `unique_id`.

        :param req: the Falcon request object.
        :param resp: the Falcon response object
        :param unique_id: the unique identifier for the Geometry.
        """
        self.check_access_level(req, resp)

        gcfg = get_global_variables()

        try:
            geometry_dict = geometry_by_unique_id(self.session, unique_id)
            self.session.commit()

            if geometry_dict is None:
                resp.status = falcon.HTTP_404
                return
            else:
                resp.status = falcon.HTTP_200

                resp.body = json.dumps(geometry_dict)

                return
        except:
            # Some sort of error occured, log
            resp.status = falcon.HTTP_500
            gcfg.logger.error("Some error occured, stack trace follows:")
            gcfg.logger.error(traceback.print_exc())
            return


class GetGeometries(M4DBService):
    r"""
    A class to encapsulate a Falcon REST service to retrieve a summary of the geometries on in the database.
    """

    def __init__(self, access_level=ACCESS_READ):
        super().__init__(access_level)

    def on_get(self, req, resp):
        """
        Retrieve summary data from the database
        :param req:  the Falcon request object /home/lnagy2/virtualenvs/m4db_server/bin/python(containing the http
                     post data sent by the user.
        :param resp: the Falcon response object.
        """
        self.check_access_level(req, resp)

        gcfg = get_global_variables()

        try:
            gcfg.logger.debug("Attempting to run SQLAlchemy query for GetGeometries")

            summary_data = geometry_summary(self.session)
            self.session.commit()


            if summary_data == []:
                # The models were empty, respond to the user
                resp.status = falcon.HTTP_404
                return
            else:
                # The models were not empty so send them back to the user
                resp.status = falcon.HTTP_200
                resp.body = json.dumps(
                    summary_data
                )
                return
        except:
            # Some sort of error occurred, log
            gcfg.logger.error("Some error occurred, stack trace follows:")
            gcfg.logger.error(traceback.print_exc())
            resp.status = falcon.HTTP_500
            return


def delete_zip_hook(req, resp, resource):
    r"""
    This is a falcon hook function, it is called after the falcon zip service to return zipped geometry data is issued
    in order to delete the temporary server side zip that must be generated since this is the data that is sent back
    to the user.

    :param req:      the falcon request object.
    :param resp:     the falcon response object.
    :param resource: the falcon resource object.
    """
    gcfg = get_global_variables()
    config = read_config_from_environ()

    gcfg.logger.debug("req.path: {}".format(req.path))

    url_split = req.path.split('/')
    unique_id = url_split[-1]

    gcfg.logger.debug("unique_id: {}".format(unique_id))

    model_tmp_zip = os.path.join(
        config["file_root"], "temporary",
        "{}.zip".format(unique_id)
    )

    try:
        os.remove(model_tmp_zip)
    except FileNotFoundError:
        # If the file could not be found ignore.
        gcfg.logger.debug("Could not delete file {} since it doesn't exist!".format(model_tmp_zip))


class GetGeometryZipByUniqueId(M4DBService):
    r"""
    A class to encapsulate a Falcon REST service that will retrieve the geometry data associated with a Geometry in the
    database (in zip form).
    """

    def __init__(self, access_level=ACCESS_READ):
        super().__init__(access_level)

    @falcon.after(delete_zip_hook)
    def on_get(self, req, resp, unique_id_zip):
        r"""
        Retrieve the information associated with a Geometry object that has the unique id given.

        :param req:           the falcon request object.
        :param resp:          the falcon response object.
        :param unique_id_zip: the unique id of the geometry in the database for which a zip should be constructed.
        """
        self.check_access_level(req, resp)

        gcfg = get_global_variables()

        unique_id = os.path.splitext(unique_id_zip)[0]

        geometry_exists = geometry_unique_id_exists(self.session, unique_id)

        try:
            if geometry_exists is None:
                resp.status = falcon.HTTP_404
                return
            else:
                zip_file = create_geometry_zip(unique_id)

                resp.status = falcon.HTTP_200
                resp.content_type = mimetypes.guess_type(zip_file)[0]
                with open(zip_file, 'rb') as fout:
                    resp.body = fout.read()
                    fout.close()
                return
        except:
            # Some sort of error occurred, log
            gcfg.logger.error("Some error occurred, stack trace follows:")
            gcfg.logger.error(traceback.print_exc())
            resp.status = falcon.HTTP_500
            return
