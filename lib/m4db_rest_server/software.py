import falcon
import json
import traceback

from sqlalchemy import text

from m4db_database.orm import ACCESS_READ
from m4db_database.orm import ACCESS_ADMIN

from m4db_database.orm import Software

from m4db_rest_server.global_config import get_global_variables

from m4db_rest_server.services import M4DBService


class GetAllSoftware(M4DBService):

    def __init__(self, access_level=ACCESS_READ):
        super().__init__(access_level)

    def on_get(self, req, resp):
        self.check_access_level(req, resp)

        gcfg = get_global_variables()

        dbsession = self.session
        software_query = dbsession.query(Software)
        softwares = software_query.all()

        try:
            if softwares == []:
                resp.status = falcon.HTTP_404
                return
            elif softwares is None:
                resp.status = falcon.HTTP_404
                return
            else:
                resp.status = falcon.HTTP_200
                software_dicts = [software.as_dict() for software in softwares]
                resp.body = json.dumps(software_dicts)
                return

        except:
            # Some sort of error occurred, log
            gcfg.logger.error("Some error occurred, stack trace follows:")
            gcfg.logger.error(traceback.print_exc())
            resp.status = falcon.HTTP_500
            return

class GetSoftwareByModelUniqueId(M4DBService):
    r"""
    A class to encapsulate a Falcon REST service to
    retrieve a list of geometries.
    """

    def __init__(self, access_level=ACCESS_READ):
        super().__init__(access_level)

    def on_get(self, req, resp, unique_id):
        """
        Retrieve summary data from the database
        :param req:       the Falcon request object /home/lnagy2/virtualenvs/m4db_server/bin/python(containing the http post data
                          sent by the user.
        :param resp:      the Falcon response object.
        """
        self.check_access_level(req, resp)

        gcfg = get_global_variables()

        try:
            gcfg.logger.debug("Attempting to run SQLAlchemy query for GetSoftwareByModelUniqueId")

            summary_cursor = self.session.execute(
                text('''
                    select 
                        model.unique_id as unique_id,
                        software.name as name,
                        software.version as version
                    from model
                    inner join metadata
                        on model.mdata_id = metadata.id
                    inner join software
                        on metadata.software_id = software.id
                    where
                        model.unique_id = '{unique_id:}'
                    '''.format(unique_id=unique_id)
                )
            )

            summary = []

            for row in summary_cursor:
                summary.append((row.unique_id,row.name,row.version))
            gcfg.logger.debug("OK!")

            if summary == []:
                # The summary was empty, respond to the user
                resp.status = falcon.HTTP_404
                return
            if len(summary) > 1:
                # The summary has more than one entry, respond to the user
                resp.status = falcon.HTTP_500
                return
            # construct list of summary data records


            resp.body=json.dumps({
                    'software_name': row.name,
                    'software_version': row.version,
                    'unique_id': row.unique_id
                })

            gcfg.logger.debug("This is the data")
            gcfg.logger.debug(summary)
            gcfg.logger.debug("")


        except:
            # Some sort of error occured, log
            gcfg.logger.error("Some error occured, stack trace follows:")
            gcfg.logger.error(traceback.print_exc())
            resp.status = falcon.HTTP_500
            return






class NewSoftware(M4DBService):

    def __init__(self, access_level=ACCESS_ADMIN):
        super().__init__(access_level)

    def on_post(self, req, resp):
        self.check_access_level(req, resp)

        gcfg = get_global_variables()
        data = req.media

        try:
            new_software = Software()
            if "name" not in data.keys():
                # Malformed request, user data failed validation
                resp.status = falcon.HTTP_400
                return
            else:
                new_software.name = data["name"]

            if "version" not in data.keys():
                # Malformed request, user data failed validation
                resp.status = falcon.HTTP_400
                return
            else:
                new_software.version = data["version"]

            if "description" in data.keys():
                new_software.description = data["description"]

            if "url" in data.keys():
                new_software.url = data["url"]

            if "citation" in data.keys():
                new_software.citation = data["citation"]

            self.session.add(new_software)
            self.session.commit()
            return

        except:
            # Some sort of error occurred, log
            gcfg.logger.error("Some error occurred, stack trace follows:")
            gcfg.logger.error(traceback.print_exc())
            resp.status = falcon.HTTP_500
            return
