r"""
This is a very simple service that may be used to test whether it is alive.
"""

import falcon

from m4db_rest_server.services import M4DBService

from m4db_database.orm import ACCESS_ALL


class Alive(M4DBService):
    r"""
    A REST service that may be used to test whether the server is alive.
    """

    def __init__(self, access_level=ACCESS_ALL):
        r"""
        Initialize service by adding the access level.
        :param access_level: the access level.
        """
        super().__init__(access_level)


    def on_get(self, req, resp):
        r"""
        The 'get' http request handler.
        """
        self.check_access_level(req, resp)
        resp.status = falcon.HTTP_200

        return
