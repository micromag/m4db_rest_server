import logging
import os
import yaml


class Configuration:
    r"""
    Global configuration object.
    """
    def __init__(self):
        # Test to see if "M4DB_CONFIG" environment variable is set.
        self.m4db_config = os.getenv('M4DB_CONFIG')
        if self.m4db_config is None:
            raise EnvironmentError("The environment variable 'M4DB_CONFIG' is not set.")

        # Test that the M4DB_CONFIG environment variable points to a valid file.
        if not os.path.isfile(self.m4db_config):
            raise ValueError("Could not find configuration file: '{}'".format(self.m4db_config))

        # Try to read the file
        with open(self.m4db_config, 'r') as fin:
            self.m4db_config_data = yaml.load(fin, Loader=yaml.FullLoader)
        
        # Standard file names
        self.model_script_merrill = 'model_script.merrill'
        self.energy_script_merrill = 'energy_script.merrill'
        self.neb_script_merrill = 'neb_script.merrill'
        self.magnetization_dat = 'magnetization.dat'
        self.magnetization_tec = 'magnetization.tec'
        self.neb_tec = 'neb.tec'
        self.magnetization_mult_tec = 'magnetization_mult.tec'
        self.magnetization_plus_tec = 'magnetization_plus.tec'
        self.magnetization_log = 'magnetization.log'
        self.magnetization = 'magnetization'
        self.model_stderr_txt = 'model_stderr.txt'
        self.model_stdout_txt = 'model_stdout.txt'
        self.neb_stderr_txt = 'neb_stderr.txt'
        self.neb_stdout_txt = 'neb_stdout.txt'
        self.energy_stderr_txt = 'energy_stderr.txt'
        self.energy_stdout_txt = 'energy_stdout.txt'
        self.geometry_cubit = 'geometry.cubit'
        self.geometry_pat = 'geometry.pat'
        self.geometry_e = 'geometry.e'
        self.geometry_stdout = 'geometry.stdout'
        self.slurm_sh = 'slurm.sh'
        self.geometry_dir_name = 'geometry'
        self.model_dir_name = 'model'
        self.neb_dir_name = 'neb'
        self.hysteresis_dir_name = 'hysteresis'
        self.forc_dir_name = 'forc'
        self.cron_dir_name = 'cron'
        self.dot_m4db = ".m4db"
        self.base_data = 'base_data'
        self.templates = 'templates'
        self.report = 'report'
        self.model_summary_txt_jinja2 = 'model_summary_txt.jinja2'
        self.model_summary_stdout_jinja2 = 'model_summary_stdout.jinja2'
        self.models_mgst_quants_stdout_jinja2 = 'models_mgst_quants_stdout.jinja2'
        self.merrill_energies_by_material_temperature_jinja2 = 'merrill_energies_by_material_temperature.jinja2'
        self.merrill_energies_jinja2 = 'merrill_energies.jinja2'
        self.merrill_model_jinja2 = 'merrill_model.jinja2'
        self.merrill_neb_child_path_jinja2 = 'merrill_neb_child_path.jinja2'
        self.merrill_neb_root_path_jinja2 = 'merrill_neb_root_path.jinja2'
        self.slurm_model_jinja2 = 'slurm_model.jinja2'
        self.slurm_neb_jinja2 = 'slurm_neb.jinja2'
        self.model_summary_txt = 'model_summary.txt'
        self.model_summary_json = 'model_summary.json'

        # Additional information
        self.lst_size_units = ['m', 'cm', 'mm', 'um', 'nm', 'pm', 'fm', 'am']

        # Some useful regular expressions
        self.rstr_hex = r'[0-9A-Fa-f]'
        self.rstr_float = r'[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?'
        self.rstr_uuid = \
            '{hx:}{{8}}-{hx:}{{4}}-{hx:}{{4}}-{hx:}{{4}}-{hx:}{{12}}'.format(
                hx=self.rstr_hex
            )

        # Some useful data formats
        self.date_time_format = "%m/%d/%Y, %H:%M:%S"

        # Database configuration info
        self.db_uri = self.m4db_config_data["db_uri"]
        self.file_root = self.m4db_config_data["file_root"]
        self.geometry_dir = os.path.join(self.file_root, self.geometry_dir_name)
        self.model_dir = os.path.join(self.file_root, self.model_dir_name)
        self.neb_dir = os.path.join(self.file_root, self.neb_dir_name)
        self.hysteresis_dir = os.path.join(self.file_root, self.hysteresis_dir_name)
        self.forc_dir = os.path.join(self.file_root, self.forc_dir_name)
        self.cron_dir = os.path.join(self.file_root, self.cron_dir_name)

        # External micromagnetics binary
        if "mm_binary" not in self.m4db_config_data.keys():
            raise ValueError("Micromagnetics binary 'mm_binary' is not set")
        else:
            self.mm_binary = self.m4db_config_data["mm_binary"]

        if "mm_binary_version" not in self.m4db_config_data.keys():
            raise ValueError("Micromagnetics binary version 'mm_binary_version' is not set")
        else:
            self.mm_binary_version = self.m4db_config_data["mm_binary_version"]

        # Logging
        if "log_logger_name" not in self.m4db_config_data.keys():
            self.m4db_config_data["log_logger_name"] = "m4db"

        if "log_level" not in self.m4db_config_data.keys():
            self.m4db_config_data["log_level"] = "INFO"

        if "log_destination" not in self.m4db_config_data.keys():
            self.m4db_config_data["log_destination"] = "stdout"

        self.logger = logging.getLogger(self.m4db_config_data["log_logger_name"])
        self.logger.setLevel(self.m4db_config_data["log_level"])
        if self.m4db_config_data["log_destination"] != 'stdout':
            self.log_handler = logging.FileHandler(self.m4db_config_data["log_destination"])
        else:
            self.log_handler = logging.StreamHandler()
        self.log_handler.setLevel(self.m4db_config_data["log_level"])
        self.logger.addHandler(self.log_handler)

        # In case we had to assign default values, we save the configuration file, overwriting the original
        with open(self.m4db_config, "w") as fout:
            yaml.dump(self.m4db_config_data, fout, default_flow_style=False)


def get_global_variables():
    if get_global_variables.gcfg is None:
        get_global_variables.gcfg = Configuration()
    return get_global_variables.gcfg


get_global_variables.gcfg = None
