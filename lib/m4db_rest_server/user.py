import falcon
import json
import traceback
import uuid

from datetime import datetime
from datetime import timedelta

from jinja2 import Environment, PackageLoader, select_autoescape

from m4db_rest_server.services import M4DBService

from m4db_database.orm import DATE_TIME_FORMAT
from m4db_database.orm import ACCESS_ALL
from m4db_database.orm import ACCESS_ADMIN
from m4db_database.orm import ACCESS_READ
from m4db_database.orm import ACCESS_WRITE

from m4db_database.utilities import password_hash

from m4db_database.orm import DBUser

from m4db_rest_server.global_config import get_global_variables

from datetime import datetime
from datetime import timedelta


class GetAllUsers(M4DBService):

    def __init__(self, access_level=ACCESS_ADMIN):
        super().__init__(access_level)

    def on_get(self, req, resp):
        self.check_access_level(req, resp)

        gcfg = get_global_variables()

        dbsession = self.session
        user_query = dbsession.query(DBUser)
        users = user_query.all()

        try:
            if users == []:
                resp.status = falcon.HTTP_404
                return
            elif users is None:
                resp.status = falcon.HTTP_404
                return
            else:
                resp.status = falcon.HTTP_200
                user_dicts = [user.as_dict() for user in users]
                resp.body = json.dumps(user_dicts)
                return

        except:
            # Some sort of error occurred, log
            gcfg.logger.error("Some error occurred, stack trace follows:")
            gcfg.logger.error(traceback.format_exc())
            resp.status = falcon.HTTP_500
            return


class NewUser(M4DBService):

    def __init__(self, access_level=ACCESS_ADMIN):
        super().__init__(access_level)

    def on_post(self, req, resp):
        self.check_access_level(req, resp)

        gcfg = get_global_variables()
        data = req.media
        try:
            new_user = DBUser()
            if "user_name" not in data.keys():
                # Malformed request, user data failed validation
                gcfg.logger.debug("'user_name' was not supplied in JSON")
                resp.status = falcon.HTTP_400
                return
            else:
                new_user.user_name = data["user_name"]

            if  "first_name" not in data.keys():
                # Malformed request, user data failed validation
                gcfg.logger.debug("'first_name' was not supplied in JSON")
                resp.status = falcon.HTTP_400
                return
            else:
                new_user.first_name = data["first_name"]

            if "initials" in data.keys():
                new_user.initials = data["initials"]

            if "email" not in data.keys():
                # Malformed request, user data failed validation
                gcfg.logger.debug("'email' was not supplied in JSON")
                resp.status = falcon.HTTP_400
                return
            else:
                new_user.email = data["email"]

            if "telephone" in data.keys():
                new_user.telephone = data["telephone"]

            if "surname" not in data.keys():
                # Malformed request, user data failed validation
                gcfg.logger.debug("'surname' was not supplied in JSON")
                resp.status = falcon.HTTP_400
                return
            else:
                new_user.surname = data["surname"]

            if "password" not in data.keys():
                # Malformed request, user data failed validation
                gcfg.logger.debug("'password' was not supplied in JSON")
                resp.status = falcon.HTTP_400
                return
            else:
                new_user.password = password_hash(data["password"])

            if "ticket_length" not in data.keys():
                # Malformed request, user data failed validation
                gcfg.logger.debug("'ticket_length' was not supplied in JSON")
                resp.status = falcon.HTTP_400
                return
            else:
                new_user.ticket_length = data["ticket_length"]

            if "access_level" not in data.keys():
                # Malformed request, user data failed validation
                gcfg.logger.debug("'access_level' was not supplied in JSON")
                resp.status = falcon.HTTP_400
                return
            else:
                new_user.access_level = data["access_level"]

            self.session.add(new_user)
            self.session.commit()
            return

        except:
            # Some sort of error occurred, log
            gcfg.logger.error("Some error occurred, stack trace follows:")
            gcfg.logger.error(traceback.format_exc())
            resp.status = falcon.HTTP_500
            return


class ForgottenPassword(M4DBService):
    r"""
    This service allows a user to retrieve a key if they've forgotten their password.
    """

    def __init__(self, access_level=ACCESS_ALL):
        super().__init__(access_level)

    def on_get(self, req, resp):
        self.check_access_level(req, resp)

        gcfg = get_global_variables()
        if "user_name" not in req.params.keys():
            resp.status = falcon.HTTP_500
            gcfg.logger.debug("No parameter 'user' given when attempting to retrieve a password change ticket hash")
            return

        user_name = req.params["user_name"]

        # Retrieve the user.
        db_user = self.session.query(DBUser).filter(DBUser.user_name == user_name).one()

        dt = timedelta(minutes=10)
        db_user.ticket_timeout = datetime.now() + dt
        db_user.ticket_hash = str(uuid.uuid4())

        self.session.commit()

        # generate a template
        template_loader = PackageLoader("m4db_rest_server", "templates")
        template_env = Environment(
            loader=template_loader,
            autoescape=select_autoescape(['jinja2'])
        )

        template = template_env.get_template(
            "forgotten_password_email.jinja2"
        )

        # Send email here
        gcfg.logger.debug(template.render(user=db_user))


class TicketHash(M4DBService):
    r"""
    This service retrieves a ticket hash for a user.
    """

    def __init__(self, access_level=ACCESS_ALL):
        super().__init__(access_level)

    def on_get(self, req, resp):
        self.check_access_level(req, resp)

        gcfg = get_global_variables()

        gcfg.logger.debug("HashTicket service 'on_get()'")

        user_name = req.get_header("user_name")
        password = req.get_header("password")

        gcfg.logger.debug("user_name: {}".format(user_name))
        gcfg.logger.debug("password: {}".format(password))

        if user_name is None or password is None:
            description = "User/password not supplied in header"
            gcfg.logger.debug(description)
            resp.status = falcon.HTTP_401
            return

        # Compute the user_name's password hash.
        pwd_hash = password_hash(password)

        # Retrieve the user
        db_user = self.session.query(DBUser).\
            filter(DBUser.user_name == user_name).\
            filter(DBUser.password == pwd_hash).\
            one_or_none()

        if db_user is None:
            gcfg.logger.info("User: {} could not be found".format(user_name))
            resp.status = falcon.HTTP_400
            return

        # Since we've retrieved the user_name, we now create a new ticket hash
        ticket_hash = str(uuid.uuid4())

        # We set the timeout value for the ticket hash
        now = datetime.now()
        dt = timedelta(minutes=db_user.ticket_length)
        ticket_timeout = now + dt

        db_user.ticket_hash = ticket_hash
        db_user.ticket_timeout = ticket_timeout

        # Return the ticket hash to the user_name
        resp.body = json.dumps({
            "ticket_hash": ticket_hash,
            "ticket_timeout": ticket_timeout.strftime(DATE_TIME_FORMAT),
            "ticket_issued": now.strftime(DATE_TIME_FORMAT)
        })
        resp.status = falcon.HTTP_200

        self.session.commit()


class ChangePassword(M4DBService):

    def __init__(self, access_level=ACCESS_ALL):
        r"""
        Anyone can change their own password.
        :param access_level: ALL
        """
        super().__init__(access_level)

    def on_post(self, req, resp):
        r"""
        Call the service. At this point the user should be authenticated.
        :param req: falcon request object.
        :param resp: falcon response object.
        :return: None
        """
        self.check_access_level(req, resp)

        gcfg = get_global_variables()

        gcfg.logger.debug("ChangePassword service 'on_post()'")

        data = req.media

        user_name = req.get_header("user_name")

        if "new_password" not in data.keys():
            # Malformed request, user data failed validation
            resp.status = falcon.HTTP_400
            return
        else:
            new_password = data["new_password"]

        db_user = self.session.query(DBUser).\
            filter(DBUser.user_name == user_name).\
            one()

        db_user.password = password_hash(new_password)

        self.session.commit()
