r"""
This is the root class for each service.
"""
import falcon

from m4db_rest_server.global_config import get_global_variables


class AuthenticationException(Exception):
    def __init__(self, message, errors=None):

        # Call the base class constructor with the parameters it needs
        super(AuthenticationException, self).__init__(message)

        # Now for your custom code...
        self.errors = errors


class M4DBService:
    r"""
    The root class of all m4db services.
    """
    def __init__(self, access_level):
        # The minimum access level required to access the service.
        self.service_minimum_access_level = access_level

    def check_access_level(self, req, resp):
        gcfg = get_global_variables()

        gcfg.logger.debug("check_access_level(): self.service_minimum_access_level: {}, user_access_level: {}".format(
          self.service_minimum_access_level, req.context.user_access_level
        ))

        if self.service_minimum_access_level > req.context.user_access_level:
            resp.set = falcon.HTTP_401
            raise AuthenticationException("User has incorrect access level for the service.")
