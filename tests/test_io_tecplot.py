import unittest

import numpy as np

import m4db_rest_server.io.tecplot
import m4db_rest_server.io.tecplot_single_phase
import m4db_rest_server.io.tecplot_multiphase


class TestIOTecplot(unittest.TestCase):

    def setUp(self):
        self.fields = [
            np.array([
                [1.0, 0.0, 0.0],
                [1.0, 0.0, 0.0],
                [1.0, 0.0, 0.0]
            ], dtype=np.float64)
        ]

        self.vertices = np.array([
            [0.0, 0.0, 0.0],
            [1.0, 0.0, 0.0],
            [0.0, 1.0, 0.0],
            [0.0, 0.0, 1.0]
        ], dtype=np.float64)

        self.elements = np.array([
            [0, 1, 2, 3]
        ], dtype=np.uint64)

        self.submesh_idxs = np.array([1], dtype=np.uint64)

        self.data = {
            "fields": self.fields,
            "vertices": self.vertices,
            "elements": self.elements,
            "submesh_idxs": self.submesh_idxs,
            "nvert": 4,
            "nelem": 1,
            "nfields": 1
        }

    def test_read_tecplot_file_single_phase(self):
        r"""
        Test whether we can correctly read a single phase tecplot file
        :return:
        """
        data = m4db_rest_server.io.tecplot.read_tecplot_file("../test_data/single_phase.tec")
        print(data)

    def test_read_tecplot_file_multiphase(self):
        r"""
        Test whether we can correctly read a multiphase tecplot file.
                :return:
        """
        data = m4db_rest_server.io.tecplot.read_tecplot_file("../test_data/multiphase.tec")
        print(data)

    def test_write_tecplot_multiphase(self):
        r"""
        Test whether we can correctly write a multiphase tecplot file.
        :return:
        """

        m4db_rest_server.io.tecplot_multiphase.write_tecplot_file("output.tec", self.data, "exsolve1_mag")





