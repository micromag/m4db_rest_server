import os
import shutil
import json
import unittest

import requests
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
from requests.packages.urllib3.exceptions import InsecureRequestWarning
from requests.packages.urllib3 import disable_warnings

from tempfile import TemporaryDirectory


class TestModelServices(unittest.TestCase):

    def setUp(self):
        disable_warnings(InsecureRequestWarning)

        self.url = "localhost"
        self.port = "8000"
        self.user_name = "lnagy2"
        self.password = "12345"

        retry = Retry(
            total=1,
            read=1,
            connect=1,
            status=0,
            status_forcelist=[]
        )

        adapter = HTTPAdapter(max_retries=retry)

        self.http_session = requests.Session()

        self.http_session.mount("https://", adapter)

        # Retrieve a ticket
        ticket_endpoint = "https://{url:}:{port:}/user/ticket-hash".format(
            url=self.url,
            port=self.port
        )
        ticket_response = self.http_session.get(
            ticket_endpoint,
            verify=False,
            headers={"user_name": self.user_name, "password": self.password}
        )
        ticket_response.raise_for_status()
        ticket_data = json.loads(ticket_response.text)
        self.ticket_hash = ticket_data["ticket_hash"]

    def test_GetModelByUniqueId(self):
        model_unique_id = "0ce1a86b-f823-461e-9b40-98e89542ea8c"

        # Next we retrieve model data by unique id
        model_endpoint = "https://{url:}:{port:}/model/{unique_id:}".format(
            url=self.url,
            port=self.port,
            unique_id=model_unique_id
        )
        model_response = self.http_session.get(
            model_endpoint,
            verify=False,
            headers={"user_name": self.user_name, "ticket_hash": self.ticket_hash}
        )
        model_response.raise_for_status()
        model_data = json.loads(model_response.text)

        self.assertEqual(model_data["unique_id"], model_unique_id)

    def test_GetModelZipByUniqueId(self):
        model_unique_id = "0ce1a86b-f823-461e-9b40-98e89542ea8c"

        # Next we retrieve model data by unique id
        model_zip_endpoint = "https://{url:}:{port:}/model/zip/{unique_id:}".format(
            url=self.url,
            port=self.port,
            unique_id=model_unique_id
        )
        model_zip_response = self.http_session.get(
            model_zip_endpoint,
            verify=False,
            headers={"user_name": self.user_name, "ticket_hash": self.ticket_hash}
        )
        model_zip_response.raise_for_status()

        with TemporaryDirectory() as tmp_dir:
            zip_file = os.path.join(
                tmp_dir,
                "{unique_id:}.zip".format(unique_id=model_unique_id)
            )
            zip_dir = os.path.join(tmp_dir, model_unique_id)
            print("Your temporary zip directory is: {}".format(zip_dir))
            with open(zip_file, "wb") as zfout:
                for chunk in model_zip_response.iter_content(chunk_size=1024):
                    zfout.write(chunk)

            shutil.unpack_archive(zip_file, zip_dir, "zip")
            for file_names in os.listdir(zip_dir):
                print(file_names)



    def test_GetModelQuantsByUniqueId(self):
        model_unique_id = "0ce1a86b-f823-461e-9b40-98e89542ea8c"

        # Next we retrieve model data by unique id
        model_quants_endpoint = "https://{url:}:{port:}/model/quants/{unique_id:}".format(
            url=self.url,
            port=self.port,
            unique_id=model_unique_id
        )
        model_quants_response = self.http_session.get(
            model_quants_endpoint,
            verify=False,
            headers={"user_name": self.user_name, "ticket_hash": self.ticket_hash}
        )
        model_quants_response.raise_for_status()
        model_quants_data = json.loads(model_quants_response.text)

        self.assertEqual(model_quants_data["unique_id"], model_unique_id)

    def test_GetModelsQuantsByMGST(self):
        running_status = "finished"
        project_name = "elongations"
        geometry = "cubo000"
        size = 100
        size_unit = "nm"
        size_convention = "esvd"

        params = {
            "material0": "magnetite",
            "temperature0": 50
        }

        # Next we retrieve model data by unique id
        models_quants_endpoint = "https://{url:}:{port:}/models/quants/{running_status:}/{user:}/{project_name:}/{geometry:}/{size:}/{size_unit:}/{size_convention:}".format(
            url=self.url,
            port=self.port,
            running_status=running_status,
            user=self.user_name,
            project_name=project_name,
            geometry=geometry,
            size=size,
            size_unit=size_unit,
            size_convention=size_convention,
        )
        models_quants_response = self.http_session.get(
            models_quants_endpoint,
            verify=False,
            headers={"user_name": self.user_name, "ticket_hash": self.ticket_hash},
            params=params
        )
        models_quants_response.raise_for_status()

        models_quants = json.loads(models_quants_response.text)
        self.assertEqual(10, len(models_quants))

    def test_GetModelsByMGST(self):
        running_status = "finished"
        project_name = "elongations"
        geometry = "cubo000"
        size = 100
        size_unit = "nm"
        size_convention = "esvd"

        params = {
            "material0": "magnetite",
            "temperature0": 50
        }

        # Next we retrieve model data by unique id
        models_endpoint = "https://{url:}:{port:}/models/{running_status:}/{user_name:}/{project_name:}/{geometry:}/{size:}/{size_unit:}/{size_convention:}".format(
            url=self.url,
            port=self.port,
            running_status=running_status,
            user_name=self.user_name,
            project_name=project_name,
            geometry=geometry,
            size=size,
            size_unit=size_unit,
            size_convention=size_convention
        )
        models_response = self.http_session.get(
            models_endpoint,
            verify=False,
            headers={"user_name": self.user_name, "ticket_hash": self.ticket_hash},
            params=params
        )
        models_response.raise_for_status()

        models = json.loads(models_response.text)

        self.assertEqual(10, len(models))

    def test_GetModelsUniqueIdsByMGST(self):
        running_status = "finished"
        project_name = "elongations"
        geometry = "cubo000"
        size = 100
        size_unit = "nm"
        size_convention = "esvd"

        params = {
            "material0": "magnetite",
            "temperature0": 50
        }

        # Next we retrieve model data by unique id
        models_endpoint = "https://{url:}:{port:}/models/unique-ids/{running_status:}/{user_name:}/{project_name:}/{geometry:}/{size:}/{size_unit:}/{size_convention:}".format(
            url=self.url,
            port=self.port,
            running_status=running_status,
            user_name=self.user_name,
            project_name=project_name,
            geometry=geometry,
            size=size,
            size_unit=size_unit,
            size_convention=size_convention
        )
        models_response = self.http_session.get(
            models_endpoint,
            verify=False,
            headers={"user_name": self.user_name, "ticket_hash": self.ticket_hash},
            params=params
        )
        models_response.raise_for_status()

        response = json.loads(models_response.text)
        unique_ids = response["unique_ids"]

        self.assertEqual(10, len(unique_ids))

    def test_GetModelSummaryByUserProject(self):
        project_name = "elongations"

        # Next we retrieve model data by unique id
        models_summary_endpoint = "https://{url:}:{port:}/models/summary/{user:}/{project_name:}".format(
            url=self.url,
            port=self.port,
            user=self.user_name,
            project_name=project_name
        )
        models_summary_response = self.http_session.get(
            models_summary_endpoint,
            verify=False,
            headers={"user_name": self.user_name, "ticket_hash": self.ticket_hash}
        )
        models_summary_response.raise_for_status()

        models_summary = json.loads(models_summary_response.text)
        print(models_summary)

    def test_SetModelRunningStatus(self):
        model_unique_id = "0ce1a86b-f823-461e-9b40-98e89542ea8c"

        # Next we retrieve model data by unique id
        model_endpoint = "https://{url:}:{port:}/model/{unique_id:}".format(
            url=self.url,
            port=self.port,
            unique_id=model_unique_id
        )
        model_response = self.http_session.get(
            model_endpoint,
            verify=False,
            headers={"user_name": self.user_name, "ticket_hash": self.ticket_hash}
        )
        model_response.raise_for_status()
        model_data = json.loads(model_response.text)

        self.assertEqual(model_data["running_status"]["name"], "finished")

        # Now update the model running status to crashed
        update_endpoint = "https://{url:}:{port:}/model/set-running-status".format(
            url=self.url,
            port=self.port
        )
        update_data = {
            "unique_id": model_unique_id,
            "new_running_status": "crashed"
        }
        update_response = self.http_session.post(
            update_endpoint,
            verify=False,
            headers={"user_name": self.user_name, "ticket_hash": self.ticket_hash},
            data=json.dumps(update_data)
        )
        update_response.raise_for_status()

        # Next we retrieve model data (again) by unique id
        model_endpoint = "https://{url:}:{port:}/model/{unique_id:}".format(
            url=self.url,
            port=self.port,
            unique_id=model_unique_id
        )
        model_response = self.http_session.get(
            model_endpoint,
            verify=False,
            headers={"user_name": self.user_name, "ticket_hash": self.ticket_hash}
        )
        model_response.raise_for_status()
        model_data = json.loads(model_response.text)

        self.assertEqual(model_data["running_status"]["name"], "crashed")

        # Now update the model running status to finished
        update_endpoint = "https://{url:}:{port:}/model/set-running-status".format(
            url=self.url,
            port=self.port
        )
        update_data = {
            "unique_id": model_unique_id,
            "new_running_status": "finished"
        }
        update_response = self.http_session.post(
            update_endpoint,
            verify=False,
            headers={"user_name": self.user_name, "ticket_hash": self.ticket_hash},
            data=json.dumps(update_data)
        )
        update_response.raise_for_status()

        # Next we retrieve model data (again) by unique id
        model_endpoint = "https://{url:}:{port:}/model/{unique_id:}".format(
            url=self.url,
            port=self.port,
            unique_id=model_unique_id
        )
        model_response = self.http_session.get(
            model_endpoint,
            verify=False,
            headers={"user_name": self.user_name, "ticket_hash": self.ticket_hash}
        )
        model_response.raise_for_status()
        model_data = json.loads(model_response.text)

        self.assertEqual(model_data["running_status"]["name"], "finished")

    def test_SetModelQuants(self):
        model_unique_id = "0ce1a86b-f823-461e-9b40-98e89542ea8c"

        # Next we retrieve model data by unique id
        model_quants_endpoint = "https://{url:}:{port:}/model/quants/{unique_id:}".format(
            url=self.url,
            port=self.port,
            unique_id=model_unique_id
        )
        model_quants_response = self.http_session.get(
            model_quants_endpoint,
            verify=False,
            headers={"user_name": self.user_name, "ticket_hash": self.ticket_hash}
        )
        model_quants_response.raise_for_status()

        original_quants = json.loads(model_quants_response.text)

        # Now update the quants
        update_endpoint = "https://{url:}:{port:}/model/set-quants".format(
            url=self.url,
            port=self.port
        )
        update_data = {
            "unique_id": model_unique_id,
            "mx_tot": 1.0,
            "my_tot": 2.0,
            "mz_tot": 3.0,
            "vx_tot": 4.0,
            "vy_tot": 5.0,
            "vz_tot": 6.0,
            "h_tot": 7.0,
            "adm_tot": 8.0,
            "e_typical": 9.0,
            "e_anis": 10.0,
            "e_ext": 11.0,
            "e_demag": 12.0,
            "e_exch1": 13.0,
            "e_exch2": 14.0,
            "e_exch3": 15.0,
            "e_exch4": 16.0,
            "e_tot": 17.0
        }
        update_response = self.http_session.post(
            update_endpoint,
            verify=False,
            headers={"user_name": self.user_name, "ticket_hash": self.ticket_hash},
            data=json.dumps(update_data)
        )
        update_response.raise_for_status()

        # Next we retrieve model data by unique id (again)
        model_quants_endpoint = "https://{url:}:{port:}/model/quants/{unique_id:}".format(
            url=self.url,
            port=self.port,
            unique_id=model_unique_id
        )
        model_quants_response = self.http_session.get(
            model_quants_endpoint,
            verify=False,
            headers={"user_name": self.user_name, "ticket_hash": self.ticket_hash}
        )
        model_quants_response.raise_for_status()

        new_quants = json.loads(model_quants_response.text)

        self.assertEqual(new_quants, update_data)

        # Put the old data back.
        update_endpoint = "https://{url:}:{port:}/model/set-quants".format(
            url=self.url,
            port=self.port
        )
        update_response = self.http_session.post(
            update_endpoint,
            verify=False,
            headers={"user_name": self.user_name, "ticket_hash": self.ticket_hash},
            data=json.dumps(original_quants)
        )
        update_response.raise_for_status()

    def test_AddNotRunModelTwoMaterials(self):
        # Next we retrieve model data by unique id
        new_model_endpoint = "https://{url:}:{port:}/model/add/not-run".format(
            url=self.url,
            port=self.port
        )

        new_model = {
            "user_name": "lnagy2",
            "project_name": "elongations",
            "repetitions": 4,
            "max_energy_evals": 10000,
            "geometry": {
                "name": "cubo000",
                "size": 100,
                "size_unit": "nm",
                "size_convention": "esvd"
            },
            "materials": [
                {
                    "name": "iron",
                    "temperature": 20,
                    "temperature_unit": "C",
                    "submesh_id": 1
                },
                {
                    "name": "magnetite",
                    "temperature": 20,
                    "temperature_unit": "C",
                    "submesh_id": 2
                }
            ],
            "start_magnetization": {
                "type": "random"
            },
            "external_field": None
        }

        update_response = self.http_session.post(
            new_model_endpoint,
            verify=False,
            headers={"user_name": self.user_name, "ticket_hash": self.ticket_hash},
            data=json.dumps(new_model)
        )
        update_response.raise_for_status()

        response_data = json.loads(update_response.text)
        unique_ids = response_data["unique_ids"]
        print(unique_ids)
        self.assertEqual(4, len(unique_ids))

    def test_AddNotRunModelInitialUniformFieldAndExternalField(self):
        # Next we retrieve model data by unique id
        new_model_endpoint = "https://{url:}:{port:}/model/add/not-run".format(
            url=self.url,
            port=self.port
        )

        new_model = {
            "user_name": "lnagy2",
            "project_name": "elongations",
            "repetitions": 4,
            "max_energy_evals": 10000,
            "geometry": {
                "name": "cubo000",
                "size": 100,
                "size_unit": "nm",
                "size_convention": "esvd"
            },
            "materials": [
                {
                    "name": "magnetite",
                    "temperature": 20,
                    "temperature_unit": "C",
                    "submesh_id": 2
                }
            ],
            "start_magnetization": {
                "type": "uniform",
                "dir_x": 1.0,
                "dir_y": 1.0,
                "dir_z": 1.0
            },
            "external_field": {
                "dir_x": 1.0,
                "dir_y": 0.0,
                "dir_z": 0.0,
                "magnitude": 30,
                "unit": "mT"
            }
        }

        update_response = self.http_session.post(
            new_model_endpoint,
            verify=False,
            headers={"user_name": self.user_name, "ticket_hash": self.ticket_hash},
            data=json.dumps(new_model)
        )
        update_response.raise_for_status()

        response_data = json.loads(update_response.text)
        unique_ids = response_data["unique_ids"]
        print(unique_ids)
        self.assertEqual(4, len(unique_ids))


    def test_AddNotRunModelInitialModelFieldAndExternalField(self):
        # Next we retrieve model data by unique id
        new_model_endpoint = "https://{url:}:{port:}/model/add/not-run".format(
            url=self.url,
            port=self.port
        )

        new_model = {
            "user_name": "lnagy2",
            "project_name": "elongations",
            "repetitions": 4,
            "max_energy_evals": 10000,
            "geometry": {
                "name": "cubo000",
                "size": 100,
                "size_unit": "nm",
                "size_convention": "esvd"
            },
            "materials": [
                {
                    "name": "magnetite",
                    "temperature": 20,
                    "temperature_unit": "C",
                    "submesh_id": 2
                }
            ],
            "start_magnetization": {
                "type": "model",
                "unique_id": "6499a00a-a39a-47e3-9fe1-cd9a859df28a"
            },
            "external_field": {
                "dir_x": 1.0,
                "dir_y": 0.0,
                "dir_z": 0.0,
                "magnitude": 30,
                "unit": "mT"
            }
        }

        update_response = self.http_session.post(
            new_model_endpoint,
            verify=False,
            headers={"user_name": self.user_name, "ticket_hash": self.ticket_hash},
            data=json.dumps(new_model)
        )
        update_response.raise_for_status()

        response_data = json.loads(update_response.text)
        unique_ids = response_data["unique_ids"]
        print(unique_ids)
        self.assertEqual(4, len(unique_ids))

    # def test_ValidateModel(self):
    #     endpoint = "https://{url:}:{port:}/model/validate/{unique_id:}".format(
    #         url=self.url,
    #         port=self.port,
    #         unique_id="0ce1a86b-f823-461e-9b40-98e89542ea8c"
    #     )
    #     response = self.http_session.get(
    #         endpoint,
    #         verify=False,
    #         headers={"user_name": self.user_name, "ticket_hash": self.ticket_hash}
    #     )
    #     response.raise_for_status()
    #
    #     print(response.text)


    # def test_PostProcessModel(self):
    #     endpoint = "https://{url:}:{port:}/model/post-process/{unique_id:}".format(
    #         url=self.url,
    #         port=self.port,
    #         unique_id="0ce1a86b-f823-461e-9b40-98e89542ea8c"
    #     )
    #     response = self.http_session.post(
    #         endpoint,
    #         verify=False,
    #         headers={"user_name": self.user_name, "ticket_hash": self.ticket_hash}
    #     )
    #     response.raise_for_status()
    #
    #     print(response.text)


    def test_GetModelMagnetizationByUniqueId(self):
        endpoint = "https://{url:}:{port:}/model/magnetization/{unique_id:}".format(
            url=self.url,
            port=self.port,
            unique_id="0ce1a86b-f823-461e-9b40-98e89542ea8c"
        )
        response = self.http_session.get(
            endpoint,
            verify=False,
            headers={"user_name": self.user_name, "ticket_hash": self.ticket_hash}
        )
        response.raise_for_status()

        print(response.text)
