import unittest

from m4db_rest_server.io.tecplot import read_tecplot_file

from m4db_rest_server.io.vtk import field_data_to_unstructured_grid
from m4db_rest_server.io.vtk import net_quantities

class TestIOTecplot(unittest.TestCase):

    def test_net_quantities_single_phase(self):
        r"""
        Read a single phase file and compute quants.
        :return:
        """

        field_data = read_tecplot_file("../test_data/single_phase.tec")
        ug = field_data_to_unstructured_grid(field_data)
        quants = net_quantities(ug)

        print(quants)


    def test_net_quantities_multiphase(self):
        r"""
        Read a multiphase file and compute quants.
        :return:
        """

        field_data = read_tecplot_file("../test_data/multiphase.tec")
        ug = field_data_to_unstructured_grid(field_data)
        quants = net_quantities(ug)

        print(quants)