import os
import shutil
import json
import unittest

import requests
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
from requests.packages.urllib3.exceptions import InsecureRequestWarning
from requests.packages.urllib3 import disable_warnings

from tempfile import TemporaryDirectory


class TestGeometryServices(unittest.TestCase):

    def setUp(self):
        disable_warnings(InsecureRequestWarning)

        self.url = "localhost"
        self.port = "8000"
        self.user_name = "lnagy2"
        self.password = "12345"

        retry = Retry(
            total=1,
            read=1,
            connect=1,
            status=0,
            status_forcelist=[]
        )

        adapter = HTTPAdapter(max_retries=retry)

        self.http_session = requests.Session()

        self.http_session.mount("https://", adapter)

        # Retrieve a ticket
        ticket_endpoint = "https://{url:}:{port:}/user/ticket-hash".format(
            url=self.url,
            port=self.port
        )
        ticket_response = self.http_session.get(
            ticket_endpoint,
            verify=False,
            headers={"user_name": self.user_name, "password": self.password}
        )
        ticket_response.raise_for_status()
        ticket_data = json.loads(ticket_response.text)
        self.ticket_hash = ticket_data["ticket_hash"]

    def test_GetGeometryByUniqueId(self):
        geometry_unique_id = "c8782d05-8d48-4cf5-b114-15cad11d1b08"

        # Next we retrieve model data by unique id
        geometry_endpoint = "https://{url:}:{port:}/geometry/{unique_id:}".format(
            url=self.url,
            port=self.port,
            unique_id=geometry_unique_id
        )
        geometry_response = self.http_session.get(
            geometry_endpoint,
            verify=False,
            headers={"user_name": self.user_name, "ticket_hash": self.ticket_hash}
        )
        geometry_response.raise_for_status()
        geometry_data = json.loads(geometry_response.text)

        self.assertEqual(geometry_data["unique_id"], geometry_unique_id)

    def test_GetGeometryZipByUniqueId(self):
        model_unique_id = "c8782d05-8d48-4cf5-b114-15cad11d1b08.zip"

        # Next we retrieve model data by unique id
        geometry_zip_endpoint = "https://{url:}:{port:}/geometry/zip/{unique_id:}".format(
            url=self.url,
            port=self.port,
            unique_id=model_unique_id
        )
        geometry_zip_response = self.http_session.get(
            geometry_zip_endpoint,
            verify=False,
            headers={"user_name": self.user_name, "ticket_hash": self.ticket_hash}
        )
        geometry_zip_response.raise_for_status()

        with TemporaryDirectory() as tmp_dir:
            zip_file = os.path.join(
                tmp_dir,
                "{unique_id:}.zip".format(unique_id=model_unique_id)
            )
            zip_dir = os.path.join(tmp_dir, model_unique_id)
            print("Your temporary zip directory is: {}".format(zip_dir))
            with open(zip_file, "wb") as zfout:
                for chunk in geometry_zip_response.iter_content(chunk_size=1024):
                    zfout.write(chunk)

            shutil.unpack_archive(zip_file, zip_dir, "zip")
            for file_names in os.listdir(zip_dir):
                print(file_names)

    def test_GetGeometries(self):
        # Next we retrieve model data by unique id
        geometry_endpoint = "https://{url:}:{port:}/geometries/summary".format(
            url=self.url,
            port=self.port
        )
        geometry_response = self.http_session.get(
            geometry_endpoint,
            verify=False,
            headers={"user_name": self.user_name, "ticket_hash": self.ticket_hash}
        )
        geometry_response.raise_for_status()
        geometry_summary = json.loads(geometry_response.text)

        self.assertEqual(len(geometry_summary), 2)

        print(geometry_summary)
