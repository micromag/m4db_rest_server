import os
import shutil
import json
import unittest

import requests
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
from requests.packages.urllib3.exceptions import InsecureRequestWarning
from requests.packages.urllib3 import disable_warnings

from tempfile import TemporaryDirectory


class TestModelServices(unittest.TestCase):

    def setUp(self):
        disable_warnings(InsecureRequestWarning)

        self.url = "localhost"
        self.port = "8000"
        self.user_name = "lnagy2"
        self.password = "12345"

        retry = Retry(
            total=1,
            read=1,
            connect=1,
            status=0,
            status_forcelist=[]
        )

        adapter = HTTPAdapter(max_retries=retry)

        self.http_session = requests.Session()

        self.http_session.mount("https://", adapter)

        # Retrieve a ticket
        ticket_endpoint = "https://{url:}:{port:}/user/ticket-hash".format(
            url=self.url,
            port=self.port
        )
        ticket_response = self.http_session.get(
            ticket_endpoint,
            verify=False,
            headers={"user_name": self.user_name, "password": self.password}
        )
        ticket_response.raise_for_status()
        ticket_data = json.loads(ticket_response.text)
        self.ticket_hash = ticket_data["ticket_hash"]

    def test_GetModelByUniqueId(self):
        model_unique_id = "0ce1a86b-f823-461e-9b40-98e89542ea8c"

        # Next we retrieve model data by unique id
        model_endpoint = "https://{url:}:{port:}/model/{unique_id:}".format(
            url=self.url,
            port=self.port,
            unique_id=model_unique_id
        )
        model_response = self.http_session.get(
            model_endpoint,
            verify=False,
            headers={"user_name": self.user_name, "ticket_hash": self.ticket_hash}
        )
        model_response.raise_for_status()
        model_data = json.loads(model_response.text)

        self.assertEqual(model_data["unique_id"], model_unique_id)

    def test_GetSoftwareByUniqueId(self):
        model_unique_id = "0ce1a86b-f823-461e-9b40-98e89542ea8c"

        # Next we retrieve model data by unique id
        model_endpoint = "https://{url:}:{port:}/software/{unique_id:}".format(
            url=self.url,
            port=self.port,
            unique_id=model_unique_id
        )
        model_response = self.http_session.get(
            model_endpoint,
            verify=False,
            headers={"user_name": self.user_name, "ticket_hash": self.ticket_hash}
        )
        model_response.raise_for_status()
        model_data = json.loads(model_response.text)
        print(model_data)
        self.assertEqual(model_data["unique_id"], model_unique_id)

