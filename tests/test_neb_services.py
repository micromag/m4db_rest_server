import os
import shutil
import json
import unittest

import requests
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
from requests.packages.urllib3.exceptions import InsecureRequestWarning
from requests.packages.urllib3 import disable_warnings

from tempfile import TemporaryDirectory

class TestNEBServices(unittest.TestCase):

    def setUp(self):
        disable_warnings(InsecureRequestWarning)

        self.url = "localhost"
        self.port = "8000"
        self.user_name = "lnagy2"
        self.password = "12345"

        retry = Retry(
            total=1,
            read=1,
            connect=1,
            status=0,
            status_forcelist=[]
        )

        adapter = HTTPAdapter(max_retries=retry)

        self.http_session = requests.Session()

        self.http_session.mount("https://", adapter)

        # Retrieve a ticket
        ticket_endpoint = "https://{url:}:{port:}/user/ticket-hash".format(
            url=self.url,
            port=self.port
        )
        ticket_response = self.http_session.get(
            ticket_endpoint,
            verify=False,
            headers={"user_name": self.user_name, "password": self.password}
        )
        ticket_response.raise_for_status()
        ticket_data = json.loads(ticket_response.text)
        self.ticket_hash = ticket_data["ticket_hash"]

    def test_GetNEBByUniqueId(self):
        neb_unique_id = "81d45982-563e-47c4-8a51-3e277751ab7f"

        # Next we retrieve NEB data by unique id
        neb_endpoint = "https://{url:}:{port:}/neb/{unique_id:}".format(
            url=self.url,
            port=self.port,
            unique_id=neb_unique_id
        )
        neb_response = self.http_session.get(
            neb_endpoint,
            verify=False,
            headers={"user_name": self.user_name, "ticket_hash": self.ticket_hash}
        )
        neb_response.raise_for_status()
        neb_data = json.loads(neb_response.text)

        self.assertEqual(neb_data["unique_id"], neb_unique_id)

    def test_GetPathRootByStartEndUniqueIds_01(self):
        model1_unique_id = "9fe5bf39-6b52-4008-9550-5ef7e9f2e686"
        model2_unique_id = "7a74f96c-4293-4bf5-95c4-de0a8cf50c66"
        neb_unique_id = "81d45982-563e-47c4-8a51-3e277751ab7f"

        # Next we retrieve NEB data by unique id
        neb_endpoint = "https://{url:}:{port:}/neb/get-path-root-by-start-end-unique-ids/{model1_unique_id:}/{model2_unique_id:}".format(
            url=self.url,
            port=self.port,
            model1_unique_id=model1_unique_id,
            model2_unique_id=model2_unique_id
        )
        neb_response = self.http_session.get(
            neb_endpoint,
            verify=False,
            headers={"user_name": self.user_name, "ticket_hash": self.ticket_hash}
        )
        neb_response.raise_for_status()
        neb_data = json.loads(neb_response.text)

        self.assertEqual(neb_data["unique_id"], neb_unique_id)

    def test_GetPathRootByStartEndUniqueIds_02(self):
        model1_unique_id = "7a74f96c-4293-4bf5-95c4-de0a8cf50c66"
        model2_unique_id = "9fe5bf39-6b52-4008-9550-5ef7e9f2e686"
        neb_unique_id = "81d45982-563e-47c4-8a51-3e277751ab7f"

        # Next we retrieve NEB data by unique id
        neb_endpoint = "https://{url:}:{port:}/neb/get-path-root-by-start-end-unique-ids/{model1_unique_id:}/{model2_unique_id:}".format(
            url=self.url,
            port=self.port,
            model1_unique_id=model1_unique_id,
            model2_unique_id=model2_unique_id
        )
        neb_response = self.http_session.get(
            neb_endpoint,
            verify=False,
            headers={"user_name": self.user_name, "ticket_hash": self.ticket_hash}
        )
        neb_response.raise_for_status()
        neb_data = json.loads(neb_response.text)

        self.assertEqual(neb_data["unique_id"], neb_unique_id)

    def test_SetNEBRunningStatus(self):
        neb_unique_id = "81d45982-563e-47c4-8a51-3e277751ab7f"

        # Retrieve NEB data by unique id
        neb_endpoint = "https://{url:}:{port:}/neb/{unique_id:}".format(
            url=self.url,
            port=self.port,
            unique_id=neb_unique_id
        )
        neb_response = self.http_session.get(
            neb_endpoint,
            verify=False,
            headers={"user_name": self.user_name, "ticket_hash": self.ticket_hash}
        )
        neb_response.raise_for_status()
        neb_data = json.loads(neb_response.text)

        self.assertEqual(neb_data["running_status"]["name"], "finished")

        # Now update the NEB running status to crashed
        update_endpoint = "https://{url:}:{port:}/neb/set-running-status".format(
            url=self.url,
            port=self.port
        )
        update_data = {
            "unique_id": neb_unique_id,
            "new_status": "crashed"
        }
        update_response = self.http_session.post(
            update_endpoint,
            verify=False,
            headers={"user_name": self.user_name, "ticket_hash": self.ticket_hash},
            data=json.dumps(update_data)
        )
        update_response.raise_for_status()

        # Next we retrieve NEB data (again) by unique id
        neb_endpoint = "https://{url:}:{port:}/neb/{unique_id:}".format(
            url=self.url,
            port=self.port,
            unique_id=neb_unique_id
        )
        neb_response = self.http_session.get(
            neb_endpoint,
            verify=False,
            headers={"user_name": self.user_name, "ticket_hash": self.ticket_hash}
        )
        neb_response.raise_for_status()
        neb_data = json.loads(neb_response.text)

        self.assertEqual(neb_data["running_status"]["name"], "crashed")

        # Now update the NEB running status to finished
        update_endpoint = "https://{url:}:{port:}/neb/set-running-status".format(
            url=self.url,
            port=self.port
        )
        update_data = {
            "unique_id": neb_unique_id,
            "new_status": "finished"
        }
        update_response = self.http_session.post(
            update_endpoint,
            verify=False,
            headers={"user_name": self.user_name, "ticket_hash": self.ticket_hash},
            data=json.dumps(update_data)
        )
        update_response.raise_for_status()

        # Next we retrieve NEB data (again) by unique id
        neb_endpoint = "https://{url:}:{port:}/neb/{unique_id:}".format(
            url=self.url,
            port=self.port,
            unique_id=neb_unique_id
        )
        neb_response = self.http_session.get(
            neb_endpoint,
            verify=False,
            headers={"user_name": self.user_name, "ticket_hash": self.ticket_hash}
        )
        neb_response.raise_for_status()
        neb_data = json.loads(neb_response.text)

        self.assertEqual(neb_data["running_status"]["name"], "finished")

    def test_AddNotRunNEBPath_with_parent(self):
        # Retrieve NEB data by unique id
        neb_endpoint = "https://{url:}:{port:}/neb/add".format(
            url=self.url,
            port=self.port
        )

        new_neb = {
            "project_name": "elongations",
            "spring_constant": 1234,
            "curvature_weight": 2222,
            "no_of_points": 121,
            "parent_unique_id": "628076cd-11bc-482f-91b9-00826651cecd",
            "calculation_type": "neb"
        }

        neb_response = self.http_session.post(
            neb_endpoint,
            verify=False,
            headers={"user_name": self.user_name, "ticket_hash": self.ticket_hash},
            data=json.dumps(new_neb)
        )
        neb_response.raise_for_status()
        neb_data = json.loads(neb_response.text)
        print(neb_data)

        # Now retrieve the neb to make sure its there
        # Next we retrieve NEB data (again) by unique id
        neb_endpoint = "https://{url:}:{port:}/neb/{unique_id:}".format(
            url=self.url,
            port=self.port,
            unique_id=neb_data["unique_id"]
        )
        neb_response = self.http_session.get(
            neb_endpoint,
            verify=False,
            headers={"user_name": self.user_name, "ticket_hash": self.ticket_hash}
        )
        neb_response.raise_for_status()
        neb_new_data = json.loads(neb_response.text)

        self.assertEqual(neb_new_data["unique_id"], neb_data["unique_id"])
        self.assertEqual(neb_new_data["spring_constant"], 1234)
        self.assertEqual(neb_new_data["curvature_weight"], 2222)
        self.assertEqual(neb_new_data["no_of_points"], 121)


    def test_AddNotRunNEBPath_without_parent(self):
        # Retrieve NEB data by unique id
        neb_endpoint = "https://{url:}:{port:}/neb/add".format(
            url=self.url,
            port=self.port
        )

        new_neb = {
            "project_name": "elongations",
            "spring_constant": 1234,
            "curvature_weight": 2222,
            "no_of_points": 121,
            "start_model_unique_id": "03b6a315-fce9-4bd2-9c5f-4f38f993d871",
            "end_model_unique_id": "cb0ca70b-cf05-4235-82ef-83b2ed1a6b9d",
            "calculation_type": "fs_heuristic"
        }

        neb_response = self.http_session.post(
            neb_endpoint,
            verify=False,
            headers={"user_name": self.user_name, "ticket_hash": self.ticket_hash},
            data=json.dumps(new_neb)
        )
        neb_response.raise_for_status()
        neb_data = json.loads(neb_response.text)
        print(neb_data)

        # Now retrieve the neb to make sure its there
        # Next we retrieve NEB data (again) by unique id
        neb_endpoint = "https://{url:}:{port:}/neb/{unique_id:}".format(
            url=self.url,
            port=self.port,
            unique_id=neb_data["unique_id"]
        )
        neb_response = self.http_session.get(
            neb_endpoint,
            verify=False,
            headers={"user_name": self.user_name, "ticket_hash": self.ticket_hash}
        )
        neb_response.raise_for_status()
        neb_new_data = json.loads(neb_response.text)

        self.assertEqual(neb_new_data["unique_id"], neb_data["unique_id"])
        self.assertEqual(neb_new_data["spring_constant"], 1234)
        self.assertEqual(neb_new_data["curvature_weight"], 2222)
        self.assertEqual(neb_new_data["no_of_points"], 121)

    def test_GetNEBPathStructureEnergiesByUniqueIds(self):
        neb_endpoint = "https://{url:}:{port:}/neb/path-structure-energies/{unique_id}".format(
            url=self.url,
            port=self.port,
            unique_id="81d45982-563e-47c4-8a51-3e277751ab7f"
        )

        response = self.http_session.get(
            neb_endpoint,
            verify=False,
            headers={"user_name": self.user_name, "ticket_hash": self.ticket_hash}
        )
        response.raise_for_status()

        path_energies = json.loads(response.text)
        print(path_energies)