#!/usr/scripts/env python

from distutils.core import setup

version = "1.0.0"

setup(name="m4db_rest_server",
      version=version,
      license="MIT",
      description="M4DB database specification and utiltiles",
      url="https://bitbucket.org/micromag/m4db_rest_server/src/master/",
      download_url="https://bitbucket.org/micromag/m4db_rest_server/get/m4db_rest_server-{}.zip".format(version),
      keywords=["micromagnetics", "database"],
      install_requires=[],
      author="L. Nagy, W. Williams",
      author_email="lnagy2@ed.ac.uk",
      packages=[
            "m4db_rest_server",
            "m4db_rest_server.utilities",
            "m4db_rest_server.utilities.validation",
            "m4db_rest_server.io"
      ],
      package_dir={
            "m4db_rest_server": "lib/m4db_rest_server",
            "m4db_rest_server.utilities": "lib/m4db_rest_server/utilities",
            "m4db_rest_server.utilities.validation": "lib/m4db_rest_server/utilities/validation",
            "m4db_reset_sever.io": "lib/m4db_rest_server/io"
      },
      package_data={
            "m4db_rest_server": ["templates/*.jinja2"]
      },
      scripts=[],
      classifiers=[
            "Development Status :: 3 - Alpha",
            "Intended Audience :: Developers",
            "Topic :: Database",
            "License :: OSI Approved :: MIT License",
            "Programming Language :: Python :: 3",
            "Programming Language :: Python :: 3.6",
            "Programming Language :: Python :: 3.7",
            "Programming Language :: Python :: 3.8",
      ]),
